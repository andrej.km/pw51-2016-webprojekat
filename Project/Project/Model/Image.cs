﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Image
    {
        public string usernameHost { get; set; }
        public int id { get; set; }
        public string imgSrc { get; set; }
        public int apartmanId { get; set; }
    }
}
