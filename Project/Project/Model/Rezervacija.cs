﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Rezervacija
    {
        public enum Status
        {
            Kreirana = 0,
            Odbijena,
            Odustanak,
            Prihvacena,
            Zavrsena
        };
        public int apartmanId { get; set; }
        public string dateTimeStart { get; set; }
        public int brojNocenja { get; set; }
        public string ukupnaCena { get; set; }
        public string usernameGest { get; set; }
        public Status status { get; set; }
    }
}
