﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class LogInClient
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
