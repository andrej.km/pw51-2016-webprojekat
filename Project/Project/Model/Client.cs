﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Client
    {
        public string username { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string sex { get; set; }
        public string role { get; set; }
        public bool blocked { get; set; } // prilikom logickog brisanja 
        public Client()
        {

        }
    }
}
