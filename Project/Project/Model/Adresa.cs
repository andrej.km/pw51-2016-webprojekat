﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Adresa
    {
        public string ulica { get; set; }
        public int broj { get; set; }
        public string naseljenoMesto { get; set; }
        public int postanskiBroj { get; set; }
    }
}
