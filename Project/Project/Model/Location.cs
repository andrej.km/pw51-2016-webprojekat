﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Location
    {
        public double lon { get; set; }
        public double lat { get; set; }
        public Adresa adresa { get; set; }
    }
}
