﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Search
    {
        // po korisniku
        public string username { get; set; }
        public string gender { get; set; }
        public string role { get; set; }

        // po apartmanu
        public string dateInterval { get; set; } // opseg datuma
        public string grad { get; set; }
        public string cenaInterval { get; set; }
        public string brSobeInterval { get; set; }
        public int brOsoba { get; set; }

        // po rezervaciji
        public string gestUsername { get; set; } // samo host i admin
    }
}
