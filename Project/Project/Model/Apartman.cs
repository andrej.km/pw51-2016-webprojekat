﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Apartman
    {
        public enum Status
        {
            Neaktivna = 0,
            Aktivna = 1
        };

        public int apartmanId { get; set; }
        public string type { get; set; }
        public int brojSoba { get; set; }
        public int brojGostiju { get; set; }
        public Location location { get; set; }
        public string datumiZaIzdavanje { get; set; }
        public string usernameHost { get; set; }
        public List<Komentar> komentari { get; set; }
        public List<Image> slike { get; set; }
        public double cenaPoNoci { get; set; }
        public Hour startHour { get; set; }
        public Hour endHour { get; set; }
        public Status status { get; set; }
        public List<ApartmanItem> amenities { get; set; }
        public List<Rezervacija> rezervacije { get; set; }
        public bool deaktiviraj { get; set; }
    }
}
