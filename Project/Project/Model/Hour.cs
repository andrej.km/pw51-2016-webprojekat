﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Hour
    {
        public int hour { get; set; }
        public string hourType { get; set; }
    }
}
