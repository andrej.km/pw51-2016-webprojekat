﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Komentar
    {
        public string gestUsername { get; set; }
        public int apartmanId { get; set; }
        public string text { get; set; }
        public int ocena { get; set; }
        public bool vidljiv { get; set; }
    }
}
