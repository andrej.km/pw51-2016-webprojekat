﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Filter
    {
        // po apartmanu
        public string apartmanAmenitie { get; set; }
        public string apartmanType { get; set; }
        public string apartmanStatus { get; set; } // samo host i admin

        // po rezervaciji
        public string rezervacijaStatus { get; set; } // samo host i admin
    }
}
