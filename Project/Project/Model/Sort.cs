﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Model
{
    public class Sort
    {
        // po apartmanu
        public string apartmanCena { get; set; }

        // po rezervaciji
        public string rezervacijaCena { get; set; }
    }
}
