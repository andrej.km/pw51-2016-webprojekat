var apartman;
var amenitieIndex;
$(document).ready(function () {
    $.get("http://localhost:3413/api/SeeApartman", function(data, status){
        apartman = JSON.parse(data);
        ispisPodataka(apartman);
    }); 
});

function ispisPodataka(apartman) {
    new Vue({
        el: '#usernameHost',
        data: {
            usernameHost: apartman.usernameHost
        }
    });
    new Vue({
        el: '#type',
        data: {
            type: apartman.type
        }
    });
    new Vue({
        el: '#brSoba',
        data: {
            brSoba: apartman.brojSoba
        }
    });
    new Vue({
        el: '#brGostiju',
        data: {
            brGostiju: apartman.brojGostiju
        }
    });
    new Vue({
        el: '#cnp',
        data: {
            cnp: apartman.cenaPoNoci
        }
    });
    new Vue({
        el: '#vzp',
        data: {
            vzp: apartman.startHour.hour
        }
    });
    new Vue({
        el: '#vzo',
        data: {
            vzo: apartman.endHour.hour
        }
    });
    if (apartman.status == 0){
        status = "Neaktivan";
    }
    else {
        status = "Aktivan";
    }
    new Vue({
        el: '#status',
        data: {
            status: status
        }
    });

    amenitieIndex = apartman.amenities.length;

    var div = document.getElementById("amenities");
    var br1 = document.createElement("br");
    div.appendChild(br1);
    for (let i = 0; i < apartman.amenities.length; i++){ // za ispis sadrzaja
        var label = document.createElement('label');
        label.innerHTML = apartman.amenities[i].naziv;
        label.id = apartman.amenities[i].id;
        div.appendChild(label);

        var button = document.createElement('button');
        button.innerHTML = 'delete';
        button.id = "btn_" + apartman.amenities[i].id;
        button.addEventListener('click', function(){
            var label = document.getElementById(this.id.split('_')[1]);
            div.removeChild(label);
            div.removeChild(this);
        });
        div.appendChild(button);       
        var br2 = document.createElement("br");
        div.appendChild(br2); 
    }
    var buttonAdd = document.createElement('button');
    buttonAdd.innerHTML = "Add amenitie";
    buttonAdd.addEventListener('click', function(){
        var br3 = document.createElement("br");
        div.appendChild(br3); 
        var input = document.createElement('input');
        input.type = 'text';
        input.id = apartman.amenities.length++;
        br3.id = input.id;
        div.appendChild(input);
        var buttonCancel = document.createElement('button');
        buttonCancel.innerHTML = "x";
        buttonCancel.id = 'btnC_' + input.id;
        buttonCancel.addEventListener('click', function(){
            var input = document.getElementById(this.id.split('_')[1]);
            div.removeChild(input);
            var break3 = document.getElementById(this.id.split('_')[1]);
            div.removeChild(break3);
            div.removeChild(this);
        });
        div.appendChild(buttonCancel);
    });
    div.appendChild(buttonAdd);

    new Vue({
        el: '#addr',
        data: {
            addr:   apartman.location.adresa.ulica + ", " + 
                    apartman.location.adresa.broj + ", " +
                    apartman.location.adresa.naseljenoMesto + ", " +
                    apartman.location.adresa.postanskiBroj
        }
    });
    var input = document.getElementById("location");
    input.style.width = "50ch";

    /// za datum
    var divDatum = document.getElementById("datumi");
    var datumi = apartman.datumiZaIzdavanje.split(',');
    for (let i = 0; i < datumi.length; i++){
        var labelDatum = document.createElement('label');
        labelDatum.innerHTML = datumi[i];
        labelDatum.id = "labelDatum_" + i;
        divDatum.appendChild(labelDatum);

        var btnDatumDelete = document.createElement('button');
        btnDatumDelete.innerHTML = 'x';
        btnDatumDelete.id = "btnDatumDelete_" + i;
        btnDatumDelete.addEventListener('click', function(){
            var labelDatumDelete = document.getElementById("labelDatum_" + this.id.split('_')[1]);
            divDatum.removeChild(labelDatumDelete);
            var breakDatumDelete = document.getElementById("breakDatum_" + this.id.split('_')[1]);
            divDatum.removeChild(breakDatumDelete);
            divDatum.removeChild(this);
        });
        divDatum.appendChild(btnDatumDelete);

        var breakDatum = document.createElement('br');
        breakDatum.id = "breakDatum_" + i;
        divDatum.appendChild(breakDatum);
    }
    var btnDodajDatum = document.createElement('button');
    btnDodajDatum.innerHTML = "Add date";
    btnDodajDatum.addEventListener('click', function(){
        var input1 = document.createElement('input');
        input1.id = "inputDate_" + dateIndex++;
        input1.placeholder = "dd-mm-yyyy";
        divDatum.appendChild(input1);
        var br4 = document.createElement("br");
        divDatum.appendChild(br4);
    });
    divDatum.appendChild(btnDodajDatum);
    var br5 = document.createElement("br");
    divDatum.appendChild(br5);

    // slike
    var divSlike = document.getElementById("sve-slike");
    for (let i = 0; i < apartman.slike.length; i++){
        var img = document.createElement('img');
        img.src =  apartman.slike[i].imgSrc;
        //console.log(img.src);
        img.id = "apartmanImg_" + indexSlike++;
        img.width = "100";
        img.height = "100";
        divSlike.appendChild(img);
        var btnObrisiSliku = document.createElement('button');
        btnObrisiSliku.id = "btnObrisiSliku_" + img.id.split('_')[1];
        btnObrisiSliku.innerHTML = "X";
        btnObrisiSliku.addEventListener('click', function(){
            var imgZaBrisanje = document.getElementById("apartmanImg_" + this.id.split('_')[1]);
            divSlike.removeChild(imgZaBrisanje);
            divSlike.removeChild(this);
        });
        divSlike.appendChild(btnObrisiSliku);
    }
}
var dateIndex = 0;
var indexSlike = 0;

// prilikom crtanja tela, da se sakrije
$(document).ready(function (){
    $("#pType").toggle();
    $("#pBrSoba").toggle();
    $("#pBrGostiju").toggle();
    $("#pCnp").toggle();
    $("#pVzp").toggle();
    $("#pVzo").toggle();
    $("#pStatus").toggle();
    $("#amenities").hide();
    $("#pAddr").toggle();
    $("#mapaa").hide();
    $("#datumi").hide();
    $("#dodaj-slike").hide();
});