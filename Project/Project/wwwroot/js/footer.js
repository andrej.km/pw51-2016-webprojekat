$(document).ready(function () {
    siteFooter();

    $(window).resize(function () {
        siteFooter();
    });

    function siteFooter() {
        var siteContent = $('#site-content');
        var siteContentHeight = siteContent.height();
        var siteContentWidth = siteContent.width();

        var siteFooter = $('#site-footer');
        var siteFooterHeight = siteFooter.height();
        var siteFooterWidth = siteFooter.width();
        
        siteContent.css({
            "margin-bottom": siteFooterHeight + 10
        });
    };
});