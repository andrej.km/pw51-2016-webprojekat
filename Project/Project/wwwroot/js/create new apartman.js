var aparman = {
    "apartmanId" : 0, // id nam predstavlja broj apartmana + 1  // odradio
    "type" : "", // odradio
    "brojSoba" : 0, // odradio
    "brojGostiju" : 0, // nzm sta tacno predstavlja, ali uradicu kao koliko je bilo do sad
    "location" : {  // odradio
        "lon" : 0,  // odradio
        "lat" : 0,   // odradio
        "adresa" : {   // odradio
            "ulica" : "",   // odradio
            "broj" : 0,   // odradio
            "naseljenoMesto" : "",    // odradio
            "postanskiBroj" : 0    // odradio
        }
    },
    "datumiZaIzdavanje" : "",  // odradio
    "usernameHost" : "",  // odradio
    "komentari" : [],   // saljemo prazno jer to korisnici zadaju  // odradio
    "slike" : [], 
    "cenaPoNoci" : 0,  // odradio
    "startHour" : 0,  // odradio
    "endHour" : 0,  // odradio
    "status" : 0 ,// inicijalno neakvina   // odradio
    "amenities" : [],  // odradio
    "rezervacije" : [] // inicijalno prazno jer to zavisi od gostiju  // odradio
};

var rezervacija = {
        "apartmanId" : 0,
        "dateTimeStart" : 0,
        "brojNocenja" : 0,
        "ukupnaCena" : 0,
        "usernameGest" : 0,
        "status" : 0
    };

var komentar = {
        "gestUsername" : "",
        "apartmanId" : 0,
        "text" : "",
        "ocena" : 0
    };

var numberOfApartmans;

var adresa;
var amenities = [];
var usernameHost;
var location;

var index = 0;

$(document).ready(function(){
    $("#back").click(function(){
        location.replace("http://localhost:3413/");
    });

    $.get("http://localhost:3413/api/Logged", function(data, status){
        if(data != "empty"){
            var d = JSON.parse(data);
            usernameHost = d.username;
        }
      });
    $('#type').on('change', function() {
        if (this.value === 'Room'){
            document.getElementById('room-number').readOnly = true;
            $('#room-number')[0].value = 1;
        }
        else{
            document.getElementById('room-number').readOnly = false;
            $('#room-number')[0].value = null;
        }
    });

    $.get("http://localhost:3413/api/NumberApartmans", function(data, status){
        aparman.apartmanId = data + 1;
    });

    $('.date').datepicker({
        multidate: true,
        format: 'dd-mm-yyyy'
    });


    $('#add-ameniti').click(function(){
        var div = document.getElementById("container-amenities");

        var input = document.createElement('input');
        input.id = index++;
        input.style.marginBottom = "5px";

        var br = document.createElement('br');

        div.appendChild(input);
        div.appendChild(br);

        // spusti prozor
        var y = $(window).scrollTop();
        $('html, body').animate({ scrollTop: y + 50 });
    });

    $("#location").hide(500);
})
$(document).ready(function(){
        var map = new ol.Map({
          target: 'map',
          layers: [
            new ol.layer.Tile({
              source: new ol.source.OSM()
            })
          ],
          view: new ol.View({
            center: ol.proj.fromLonLat([19.833549,45.267136]),
            zoom: 16
          })
        });
        
        function reverseGeocode(coords) {
            aparman.location.lon = coords[0];
            aparman.location.lat = coords[1];
     fetch('http://nominatim.openstreetmap.org/reverse?format=json&lon=' + coords[0] + '&lat=' + coords[1])
       .then(function(response) {
        //console.log(response);
              return response.json();
          }).then(function(json) {
            //console.log(json);
            $("#location").show(500);
            $("#location")[0].value = (`${json.address.road}, ${json.address.house_number}, ${json.address.city}, ${json.address.postcode} `);
            // spusti prozor
            var y = $(window).scrollTop();
            $('html, body').animate({ scrollTop: y + 50 });
            // ne zelimo ne definisano polje
            if (json.address.road == undefined ||
                json.address.house_number == undefined ||
                json.address.city == undefined ||
                json.address.postcode == undefined){
                
                    setTimeout(function(){
                        $("#location")[0].value = "Adresa mora biti potpuno poznata!";
                        $("#location").css("color", "red");
                    }, 2000);
            }
            else{
                $("#location").css("color", "black");
                adresa = {
                    "ulica" : json.address.road,
                    "broj" : json.address.house_number,
                    "naseljenoMesto" : json.address.city,
                    "postanskiBroj" : json.address.postcode
                };
                aparman.location.adresa = adresa;
            }
          });
  }
  
        map.on('click', function(evt) {
            var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
            var lon = lonlat[0];
            var lat = lonlat[1];
            var coord = ol.proj.toLonLat(evt.coordinate);
            reverseGeocode(coord);
            makeMarker(lonlat);
      });
      
          function makeMarker(lonlat){
        //console.log(lonlat);
              var marker = new ol.Feature({
          geometry: new ol.geom.Point(
          ol.proj.fromLonLat(lonlat))
        });
  
        var vectorSource = new ol.source.Vector({
          features: [marker]
        });
  
        var markerVectorLayer = new ol.layer.Vector({
          source: vectorSource,
        });
        map.addLayer(markerVectorLayer);
  
          }
});

// za dugme kreate
$(document).ready(function(){
    $("#create").click(function(){
        validate();
        // uzimanje svih sadrzaja stana
        for (let i = 0; i < document.getElementById("container-amenities").childElementCount / 2; i++){
            var input = $(`#${i}`)[0].value;
            amenities.push({"id" : i, "naziv": input});
        }

        aparman.type = $("#type")[0].value;
        aparman.brojSoba = $("#room-number")[0].value;
        aparman.brojGostiju = $("#gests-number")[0].value;
        aparman.cenaPoNoci = $("#prise")[0].value;
        aparman.startHour = {
            "hour" : $("#start-hour")[0].value,
            "hourType" : $("#typeHour1")[0].value
        };
        aparman.endHour = {
            "hour" : $("#end-hour")[0].value,
            "hourType" : $("#typeHour2")[0].value
        };
        aparman.usernameHost = usernameHost;
        aparman.amenities = amenities;
        aparman.datumiZaIzdavanje = $("#datumiZaIzadavanje")[0].value;
        aparman.slike = images;
        
        if (validate()){
            alert(error);
        }
        else{
            //console.log(aparman);
            saveApartman(aparman);
            var btn = document.getElementById("create");
            btn.style.display = "none";
        }
    });
});

var error = "";
function validate(){
    var retBool = false;
    error = "";
    if (aparman.brojSoba == ""){
        error += "Enter a number of room's!\n";
        retBool = true;
    }
    if (aparman.brojGostiju == ""){
        error += "Enter a number of gests's!\n";
        retBool = true;
    }
    if (aparman.amenities == ""){
        error += "Enter at least one amenitie!\n";
        retBool = true;
    }
    if (aparman.cenaPoNoci == ""){
        error += "Enter prise of apartman/room!\n";
        retBool = true;
    }
    if (aparman.datumiZaIzdavanje == ""){
        error += "Enter dates!\n";
        retBool = true;
    }
    if (aparman.endHour == ""){
        error += "Enter hour for check out!\n";
        retBool = true;
    }
    if (aparman.location == ""){
        error += "Choose location!\n";
        retBool = true;
    }
    if (aparman.slike == ""){
        error += "Choose at least one picture!\n";
        retBool = true;
    }
    if (aparman.startHour == ""){
        error += "Enter hour for check in!\n";
        retBool = true;
    }
    if (aparman.type == ""){
        error += "Choose type!\n";
        retBool = true;
    }
    return retBool;
}

// za slike
var imageIndex = 0;
$(document).ready(function(){
    function readURL(input) {
        
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function(e) {
            var div = document.getElementById("container-slika");
            var img = document.createElement('img');
            img.src =  e.target.result;
            img.id = "loaded-img-" + imageIndex++;
            img.width = "100";
            img.height = "100";
            div.appendChild(img);

            var image = {
                "usernameHost" : "",
                "id" : 0,
                "imgSrc" : "",
                "apartmanId" : 0
            }

            image.usernameHost = usernameHost;
            image.id = indexSlike++;
            image.imgSrc = img.src;
            image.apartmanId = aparman.apartmanId;
            images.push(image);
            console.log(images);
          }
          
          reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });
});

var indexSlike = 0;

var images = [];

function saveApartman(aparman){
    $.ajax({  
        url: 'http://localhost:3413/api/Apartman',  
        type: 'POST',  
        dataType: 'json',  
        data: aparman
    });  
}