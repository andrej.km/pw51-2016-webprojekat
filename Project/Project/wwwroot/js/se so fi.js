var loggedClient;
var odradioUcitavenjeUlogovanog = false;
$(document).ready(function(){
    $.get("http://localhost:3413/api/Logged", function(data, status){
        if (odradioUcitavenjeUlogovanog == false){
            if (data != "empty") {
                loggedClient = JSON.parse(data);
                if (loggedClient.role == "gest"){
                    sakrij();
                }
            }
            else{
                loggedClient = "empty";
                sakrij();
            }
            odradioUcitavenjeUlogovanog = true;
        }   
    })
});

$(document).ready(function(){
    //alert();
    $("#search-btn").click(function(){
        $("#search").toggle(500);
        var x = document.getElementById("sort");
        var x1 = document.getElementById("filter");
        if (x.style.display === "block" || x1.style.display === "block") {
            x.style.display = "none";
            x1.style.display = "none";
        }
    });

    $("#sort-btn").click(function(){
        $("#sort").toggle(500);
        var x = document.getElementById("search");
        var x1 = document.getElementById("filter");
        if (x.style.display === "block" || x1.style.display === "block") {
            x.style.display = "none";
            x1.style.display = "none";
        }
    });

    $("#filter-btn").click(function(){
        $("#filter").toggle(500);
        var x = document.getElementById("sort");
        var x1 = document.getElementById("search");
        if (x.style.display === "block" || x1.style.display === "block") {
            x.style.display = "none";
            x1.style.display = "none";
        }
    });

    $("#send-search").click(function(){
        var search = {
            "username" : $("#search-username")[0].value,
            "gender" : $("#search-gender")[0].value,
            "role" : $("#search-role")[0].value,
            "dateInterval" : $("#search-date-start")[0].value + "_" + $("#search-date-end")[0].value, 
            "grad" : $("#search-grad")[0].value,
            "cenaInterval" : $("#search-cena-start")[0].value + "_" + $("#search-cena-end")[0].value,
            "brSobeInterval" : $("#search-broj-soba-start")[0].value + "_" + $("#search-broj-soba-end")[0].value,
            "brOsoba" : $("#search-broj-osoba")[0].value,
            "gestUsername" : $("#search-gest")[0].value
        };
        console.log($("#search-gest")[0].value);
        sendSearch(search);
    });

    $("#send-sort").click(function(){
        var radios = document.getElementsByName('as');

        for (var i = 0, length = radios.length; i < length; i++){
            if (radios[i].checked){
                if (radios[i].value == "apartman"){
                    var sort = {
                        "apartmanCena" : $("#sort-apartman-sort")[0].value
                    }
                    sendSort(sort);
                }
                else {
                    var sort = {
                        "rezervacijaCena" : $("#sort-rezervation-sort")[0].value
                    }
                    sendSort(sort);
                }
                break;
            }
        }
    });

    $("#send-filter").click(function(){
        var radios = document.getElementsByName('fi');

        for (var i = 0, length = radios.length; i < length; i++){
            if (radios[i].checked){
                if (radios[i].value == "apartman" || loggedClient.role == "gest"){
                    var filter = {
                        "apartmanAmenitie" : $("#filter-sadrzaj")[0].value,
                        "apartmanType" : $("#filter-type")[0].value,
                        "apartmanStatus" : $("#filter-apartman-filter")[0].value
                    }
                    sendFilter(filter);
                }
                else {
                    var filter = {
                        "rezervacijaStatus" : $("#filter-rezervation-filter")[0].value
                    }
                    sendFilter(filter);
                }
                break;
            }
        }
    });
});

function sakrij(){
    var l1 = document.getElementById("l1sakrij").style.display = "none";
    var input1 = document.getElementById("search-gest").style.display = "none";

    var l2 = document.getElementById("l2sakrij").style.display = "none";
    var select1 = document.getElementById("filter-apartman-filter").style.display = "none";

    var l2 = document.getElementById("l3sakrij").style.display = "none";
    var select2 = document.getElementById("filter-rezervation-filter").style.display = "none";

    var radio = document.getElementsByName("fi");//.style.display = "none";
    radio[0].style.display = "none";
    radio[1].style.display = "none";
}

var allDataFromServer = [];
var odradioSearch = false;
function sendSearch(search){
    $.post("http://localhost:3413/api/Search",search,
        function(data,status){
            if (odradioSearch == false){
                if (data != ""){
                    var all = data.split('?');
                    all.pop();
                    for (let i = 0; i < all.length; i++){
                        var temp = JSON.parse(all[i]);
                        allDataFromServer.push(temp);
                    }
                    hide("sort-btn", "filter-btn", "send-search");
                    showAllData(allDataFromServer);
                }
                else{
                    hide("sort-btn","filter-btn");
                    alert("No data");
                }
                //console.log(data);
                
               /* for (let i = 0 ; all.length - 1; i++){
                    console.log(all[i]);
                }*/
                odradioSearch = true;
            }
        });
}

var odradioSort = false;
function sendSort(sort){
    $.post("http://localhost:3413/api/Sort",sort,
    function(data,status){
        if (odradioSort == false){
            if (data != ""){
                var all = data.split('?');
                all.pop();
                for (let i = 0; i < all.length; i++){
                    var temp = JSON.parse(all[i]);
                    allDataFromServer.push(temp);
                }
                hide("search-btn", "filter-btn", "send-search");
                showAllDataSort(allDataFromServer);
            }
            else{
                hide("search-btn","filter-btn");
                alert("No data");
            }
            //console.log(data);
            
           /* for (let i = 0 ; all.length - 1; i++){
                console.log(all[i]);
            }*/
            odradioSort = true;
        }
    });
}

var odradioFilter = false;
function sendFilter(filter){
    $.post("http://localhost:3413/api/Filter",filter,
    function(data,status){
        if (odradioFilter == false){
            if (data != ""){
                var all = data.split('?');
                all.pop();
                for (let i = 0; i < all.length; i++){
                    var temp = JSON.parse(all[i]);
                    allDataFromServer.push(temp);
                }
                hide("search-btn", "sort-btn", "send-search");
                showAllData(allDataFromServer);
            }
            else{
                hide("search-btn","sort-btn");
                alert("No data");
            }
            //console.log(data);
            
           /* for (let i = 0 ; all.length - 1; i++){
                console.log(all[i]);
            }*/
            odradioFilter = true;
        }
    }); 
}

function hide(button1, button2, button3){
    document.getElementById(button1).style.display = "none";
    document.getElementById(button2).style.display = "none";
}

function showAllData(allData){
    var divKlijenti = document.getElementById("klijenti");
    var divApartmani = document.getElementById("apartmani");
    var divRezervacije = document.getElementById("rezervacije");

    var clients = [];
    var apartmans = [];
    var rezervations = [];
    for (let i = 0; i < allData.length; i++){
        if (loggedClient.role == "host"){
            if (allData[i].usernameHost == loggedClient.username){
                var temp = new Object;
                temp = allData[i].username;
                if (temp == undefined){
                    var temp1 = new Object;
                    temp1 = allData[i].datumiZaIzdavanje;
                    if (temp1 == undefined){
                        rezervations.push(allData[i]);
                    }
                    else{
                        apartmans.push(allData[i]);
                    }
                }
                else{
                    clients.push(allData[i]);
                }
            }
        }
        else{
            var temp = new Object;
            temp = allData[i].username;
            if (temp == undefined){
                var temp1 = new Object;
                temp1 = allData[i].datumiZaIzdavanje;
                if (temp1 == undefined){
                    rezervations.push(allData[i]);
                }
                else{
                    apartmans.push(allData[i]);
                }
            }
            else{
                clients.push(allData[i]);
            }
        }
        
    }

    for (let c = 0; c < clients.length; c++){
        var label = document.createElement('label');
        label.innerHTML = clients[c].username + ", " + clients[c].name + ", " + clients[c].surname;
        divKlijenti.appendChild(label);

        var br = document.createElement('br');
        divKlijenti.appendChild(br);
    }
    
    for (let a = 0; a < apartmans.length; a++){
        var label1 = document.createElement('label');
        label1.innerHTML = apartmans[a].usernameHost + ", cena po noci : " + apartmans[a].cenaPoNoci + 
        ", broj soba : " + apartmans[a].brojSoba;
        divApartmani.appendChild(label1);

        var br1 = document.createElement('br');
        divApartmani.appendChild(br1);
    }
    for (let a = 0; a < rezervations.length; a++){
        console.log(rezervations[a]);
        var label1 = document.createElement('label');
        label1.innerHTML = rezervations[a].dateTimeStart + ", status : " + rezervations[a].status + 
        ", ukupna cena : " + rezervations[a].ukupnaCena;
        divApartmani.appendChild(label1);

        var br1 = document.createElement('br');
        divApartmani.appendChild(br1);
    }
}

function showAllDataSort(allData){
    var divApartmani = document.getElementById("apartmani");

    var clients = [];
    var apartmans = [];
    var rezervations = [];
    for (let i = 0; i < allData.length; i++){
        if (loggedClient.role == "host"){
            if (allData[i].usernameHost == loggedClient.username){
                apartmans.push(allData[i]);
            }
        }
        else{
            apartmans.push(allData[i]);
        }
    }
    
    for (let a = 0; a < apartmans.length; a++){
        var label1 = document.createElement('label');
        label1.innerHTML = apartmans[a].usernameHost + ", cena po noci : " + apartmans[a].cenaPoNoci + 
        ", broj soba : " + apartmans[a].brojSoba;
        divApartmani.appendChild(label1);
        
        if (apartmans[a].rezervacije != null){
            var divRe = document.createElement('div');
            divRe.style.border = "thin 2px solid";
            
            for (let aa = 0; aa < apartmans[a].rezervacije.length; aa++){
                //console.log(aa);
                //console.log(apartmans[a].rezervacije[aa]);
                var label11 = document.createElement('label');
                label11.innerHTML = " - " + apartmans[a].rezervacije[aa].dateTimeStart + 
                ", status : " + apartmans[a].rezervacije[aa].status + 
                ", ukupna cena : " + apartmans[a].rezervacije[aa].ukupnaCena;
                divRe.appendChild(label11);
        
                var br11 = document.createElement('br');
                divRe.appendChild(br11);
            }
            divApartmani.appendChild(divRe);
        }
        
        

        var br1 = document.createElement('br');
        divApartmani.appendChild(br1);
    }
    
}