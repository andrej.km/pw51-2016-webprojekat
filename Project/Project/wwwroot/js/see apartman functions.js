var apartman;
var deaktivirajBtn = false;
$(document).ready(function(){
    $.get("http://localhost:3413/api/SeeApartman", function(data, status){ // moram zbog apartmanId
        apartman = JSON.parse(data);
        //console.log(apartman);
    }); 
    $("#btnDeaktiviraj1").click(function(){
        if (apartman.deaktiviraj == false){
            apartman.deaktiviraj = true;
            sendApartman(apartman);
            alert("Apartman je deaktiviran!");
        }
        else{
            apartman.deaktiviraj = false;
            sendApartman(apartman);
            alert("Apartman je aktiviran!");
        }
        
        document.getElementById("btnDeaktiviraj1").style.display = "none";
        document.getElementById("btSendChange").style.display = "none";
    });

    $("#aa").click(function(){
        $("html, body").animate({ scrollTop: 0 }, "slow");
        $("#pType").toggle(500);
        $("#pBrSoba").toggle(500);
        $("#pBrGostiju").toggle(500);
        $("#pCnp").toggle(500);
        $("#pVzp").toggle(500);
        $("#pVzo").toggle(500);
        $("#pStatus").toggle(500);
        var x = document.getElementById("amenities");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        $("#pAddr").toggle(500);
        var x = document.getElementById("mapaa");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
        var x1 = document.getElementById("datumi");
        if (x1.style.display === "none") {
            x1.style.display = "block";
        } else {
            x1.style.display = "none";
        }

        var x2 = document.getElementById("dodaj-slike");
        if (x2.style.display === "none") {
            x2.style.display = "block";
        } else {
            x2.style.display = "none";
        }
    });
});

function deaktiviraj(){
    alert();
}

$(document).ready(function(){
    $("#btSendChange").click(function(){
        
        var postApartman = getAllData();

        sendApartman(postApartman);

        alert("Changes are saved!");
        //window.load("http://localhost:3413/");
    });
})

function getAllData(){
    // kupljenje sadrzaja stana
    var postAmenities = [];
    var divAmenities = document.getElementById('amenities');
    var takenAmenities = divAmenities.getElementsByTagName("label");
    var i = 0
    for (; i < takenAmenities.length; i++){
        var amenitieTemp = new Object;
        amenitieTemp.id = i;
        amenitieTemp.naziv = takenAmenities[i].innerHTML;
        postAmenities.push(amenitieTemp);
    }
    var newAmenities = divAmenities.getElementsByTagName("input");
    for (let tempI = 0, j = i; tempI < newAmenities.length; j++, tempI++){
        var amenitieTemp = new Object;
        amenitieTemp.id = j;
        amenitieTemp.naziv = newAmenities[tempI].value;
        postAmenities.push(amenitieTemp);
    }

    // kupljenje sata
    var startH = new Object;
    startH.hour = document.getElementById('lVzp').innerHTML;
    startH.hourType = apartman.startHour.hourType;

    var endH = new Object;
    endH.hour = document.getElementById('lVzo').innerHTML;
    endH.hourType = apartman.endHour.hourType; 

    // ako nije menjana lokacija
    if (postLocation.lat === 0){
        postLocation = apartman.location;
    }
    // datumi
    var postDatumi = "";
    var divDatumi = document.getElementById('datumi');
    var takenDatumi = divDatumi.getElementsByTagName("label");
    var i = 0
    for (; i < takenDatumi.length; i++){
        var datumTemp = takenDatumi[i].innerHTML;
        if (i === takenDatumi.length - 1){
            postDatumi += datumTemp;
        }
        else{
            postDatumi += datumTemp + ',';
        }
    }
    var newDates = divDatumi.getElementsByTagName("input");
    for (let tempI = 0, j = i; tempI < newDates.length; j++, tempI++){
        if (tempI === 0){
            postDatumi += ",";
        }
        var datumTemp = newDates[tempI].value;
        if (tempI === newDates.length - 1){
            postDatumi += datumTemp;
        }
        else{
            postDatumi += datumTemp + ',';
        }
    }

    // slike
    var loadedImages = document.getElementById("sve-slike");
    var allLoadedImages = loadedImages.getElementsByTagName("img");
    for (let t = 0; t < allLoadedImages.length; t++){
        var imgTemp = new Object;
        imgTemp.usernameHost = apartman.usernameHost;
        imgTemp.id = indexSlike++;
        imgTemp.imgSrc = allLoadedImages[t].currentSrc;
        imgTemp.apartmanId = apartman.apartmanId;
        images.push(imgTemp);
    }

    
    var brojSoba;
    if($("#type")[0].value == "Room"){
        brojSoba = 1;
    }
    else{
        brojSoba = document.getElementById('lBrSoba').innerHTML;
    }

    var status;
    if (document.getElementById('lStatus').innerHTML === "Aktivan"){
        status = 1;
    }
    else{
        status = 0;
    }

    var postApartman={
        "apartmanId" : apartman.apartmanId,
        "type" : document.getElementById('lType').innerHTML,
        "brojGostiju" : document.getElementById('lBrGostiju').innerHTML,
        "brojSoba" : brojSoba,
        "status" : status,
        "usernameHost" : apartman.usernameHost,
        "amenities" : postAmenities,
        "startHour" : startH,
        "endHour" : endH,
        "komentari" : [],
        "rezervacije" : [], 
        "cenaPoNoci" : document.getElementById('lCnp').innerHTML,
        "location" : postLocation,
        "datumiZaIzdavanje" : postDatumi,
        "deaktiviraj" : apartman.deaktiviraj,
        "slike" : images
    }
    //console.log(postApartman);
    return postApartman;
}

function sendApartman(apartman){
    $.ajax({  
        url: 'http://localhost:3413/api/SaveChangedApartman',  
        type: 'POST',  
        dataType: 'json',  
        data: apartman
    });  
}

// map
var postLocation = {
    "adresa" : {},
    "lon" : 0,
    "lat" : 0
};
$(document).ready(function(){
    var map = new ol.Map({
      target: 'map',
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([19.833549,45.267136]),
        zoom: 16
      })
    });
    
    function reverseGeocode(coords) {
 fetch('http://nominatim.openstreetmap.org/reverse?format=json&lon=' + coords[0] + '&lat=' + coords[1])
   .then(function(response) {
    //console.log(response);
          return response.json();
      }).then(function(json) {
        //console.log(json);
        $("#location").show(500);
        $("#location")[0].value = (`${json.address.road}, ${json.address.house_number}, ${json.address.city}, ${json.address.postcode} `);
        // spusti prozor
        var y = $(window).scrollTop();
        $('html, body').animate({ scrollTop: y + 50 });
        // ne zelimo ne definisano polje
        if (json.address.road == undefined ||
            json.address.house_number == undefined ||
            json.address.city == undefined ||
            json.address.postcode == undefined){
            
                setTimeout(function(){
                    $("#location")[0].value = "Adresa mora biti potpuno poznata!";
                    $("#location").css("color", "red");
                }, 2000);
        }
        else{
            $("#location").css("color", "black");
            postLocation.adresa = {
                "ulica" : json.address.road,
                "broj" : json.address.house_number,
                "naseljenoMesto" : json.address.city,
                "postanskiBroj" : json.address.postcode
            };
        }
      });
}

    map.on('click', function(evt) {
        var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');
        postLocation.lon = lonlat[0];
        postLocation.lat = lonlat[1];
        var coord = ol.proj.toLonLat(evt.coordinate);
        reverseGeocode(coord);
        makeMarker(lonlat);
  });
  
      function makeMarker(lonlat){
    //console.log(lonlat);
          var marker = new ol.Feature({
      geometry: new ol.geom.Point(
      ol.proj.fromLonLat(lonlat))
    });

    var vectorSource = new ol.source.Vector({
      features: [marker]
    });

    var markerVectorLayer = new ol.layer.Vector({
      source: vectorSource,
    });
    map.addLayer(markerVectorLayer);

      }
});


// za slike
var images = [];
var imageIndex = 0;
$(document).ready(function(){
    
    function readURL(input) {
        
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          
          reader.onload = function(e) {
            var div = document.getElementById("container-slika");
            var img = document.createElement('img');
            img.src =  e.target.result;
            img.id = "loaded-img-" + imageIndex++;
            img.width = "100";
            img.height = "100";
            div.appendChild(img);

            var imgTempAdd = new Object; 
            imgTempAdd.usernameHost = apartman.usernameHost;
            imgTempAdd.id = indexSlike++;
            imgTempAdd.imgSrc = e.target.result;
            imgTempAdd.apartmanId = apartman.apartmanId;
            images.push(imgTempAdd);
            //console.log(images);
          }
          
          reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function() {
        readURL(this);
    });
});

var indexSlike = 0;