//#region Fields
var loggedClient = {};
var odradioApiHostApartmans = false;
var apartmans = [];
var allApartmans = [];
var odradioUcitavenjeUlogovanog = false;
var odradioGetAllApartmans = false;
var filter = false;
//#endregion

//#region AJAX
$(document).ready(function(){
        $.get("http://localhost:3413/api/Logged", function(data, status){
            if (odradioUcitavenjeUlogovanog == false){
            if (data != "empty") {
                loggedClient = JSON.parse(data);
                showSection1(loggedClient.role);
                if (loggedClient.role === "host"){
                    getAllApartmansOfHost();
                    $("#section1h3")[0].innerHTML = "functions";
                    $("#section2h3")[0].innerHTML = "active aparmans";
                    $("#section3h3")[0].innerHTML = "not active aparmans";
                    $("#section4h3")[0].innerHTML = "logicki obrisani";
                }
                else if (loggedClient.role === "administrator"){
                    getAllApartmans();
                    $("#section1h3")[0].innerHTML = "functions";
                    $("#section2h3")[0].innerHTML = "active aparmans";
                    $("#section3h3")[0].innerHTML = "not active aparmans";
                    $("#section4h3")[0].innerHTML = "logicki obrisani";
                }
                else if (loggedClient.role === "gest"){
                    getAllApartmans();
                    $("#section1h3")[0].innerHTML = "search/sort/filter";
                    $("#section2h3")[0].innerHTML = "active aparmans";
                }
            }
            else {  // niko nije ulogovan!
                loggedClient = "empty";
                showSection1("empty")
                getAllApartmans();
                $("#section1h3")[0].innerHTML = "functions";
                $("#section2h3")[0].innerHTML = "active aparmans";
            }
            odradioUcitavenjeUlogovanog = true; 
        }
    });
});
//#endregion

//#region ADMIN + GEST + NOT LOGGED
function getAllApartmans(){
    $.get("http://localhost:3413/api/AllApartmans", function(data, status){
        if (odradioGetAllApartmans == false){
            odradioGetAllApartmans = true;
            allApartmans = data.split('\n');
            allApartmans.pop(); // da sklonimo prazan 
            for (var i = 0; i < allApartmans.length; i++){
                apartmans.push(JSON.parse(allApartmans[i]));
                var tempApartman = JSON.parse(allApartmans[i]);
                if (tempApartman.deaktiviraj == true){
                    if (loggedClient.role === "administrator"){
                        show1(i, tempApartman, "s4id01");
                    }
                }
                else{
                    if (tempApartman.status == 1){
                        if (loggedClient.role === "administrator" ||
                            loggedClient.role === "gest" ||
                            loggedClient === "empty"){
                            show1(i, tempApartman, "s2id01");
                            }
                    }
                    else{
                        if (loggedClient.role === "administrator"){
                            show1(i, tempApartman, "s3id01");
                        }
                    }
                }
            }
        }
    });
}
//#endregion

//#region HOST
function getAllApartmansOfHost(){
    $.post("http://localhost:3413/api/HostApartmans",loggedClient.username,
        function(data,status){
            if (odradioApiHostApartmans === false){
                //console.log(data.length);
                for (var i = 0; i < data.length; i++){
                    //console.log(i);
                    apartmans.push(JSON.parse(data[i]));
                    if (apartmans[i].deaktiviraj == true){
                        if (apartmans[i].usernameHost == loggedClient.username){
                            show1(i, apartmans[i], "s4id01");
                        }
                    }
                    else{
                        if (apartmans[i].status == 1){
                            if (apartmans[i].usernameHost == loggedClient.username){
                                show1(i, apartmans[i], "s2id01");
                            }
                        }
                        else{
                            if (apartmans[i].usernameHost == loggedClient.username){
                                show1(i, apartmans[i], "s3id01");
                            }
                        }
                    }
                }
                odradioApiHostApartmans = true;
            }
    });
}
//#endregion

//#region GEST

//#endregion

//#region NOT LOGGED

//#endregion

//#region Section1
function showSection1(role){
    //console.log(role);
    if (role === "administrator"){
        ReactDOM.render(
            <div>
                <a href="html/registration.html">Create host</a>
                <br></br>
                <a href="html/all clients.html">See all clients</a>
                <br></br>
                <a href="html/search sort filter.html">Search/sort/filter</a>
                <br></br>
                <a href="html/all rezervations.html">See all rezervations</a>
                <br></br>
                <a href="html/komentari.html">See all comments</a>
                <br></br>
                <a href="html/add amenities.html">Add amenities</a>
            </div>,
            document.getElementById('s1id01')
        );
    }
    else if (role === "host"){
        ReactDOM.render(
            <div>
                <a href="html/create apartman.html">Create apartman</a>
                <br></br>
                <a href="html/search sort filter.html">Search/sort/filter</a>
                <br></br>
                <a href="html/all rezervations.html">See all rezervations</a>
                <br></br>
                <a href="html/komentari.html">See all comments</a>
            </div>,
            document.getElementById('s1id01')
        );
    }
    else if (role === "gest"){
        ReactDOM.render(
            <div>
                <a href="html/search sort filter.html">Search/sort/filter</a>
                <br></br>
                <a href="html/all rezervations.html">See all rezervations</a>
            </div>,
            document.getElementById('s1id01')
        );
    }
    else if (role === "empty"){
        ReactDOM.render(
            <div>
                <a href="html/search sort filter.html">Search/sort/filter</a>
            </div>,
            document.getElementById('s1id01')
        );
    }
}
//#endregion

//#region Methods
function show1(index, apartman, section){
    //console.log(apartman);
    var div_s2id01 = document.getElementById(section);

    //pravljenje novog
    var div = document.createElement('div');
    div.style.width = "100%";
    div.style.height = "300px";
    div.style.border = "1px solid black";
    div.style.marginBottom = '5px';

    var img = document.createElement('img');
    img.src =  apartman.slike[0].imgSrc;
    img.width = "400";
    img.height = "300";
    img.style.cssFloat = 'left';
    div.appendChild(img);

    var br1 = document.createElement('br');
    div.appendChild(br1);

    var label1 = document.createElement('label');
    label1.innerHTML = apartman.type;
    div.appendChild(label1);

    var label2 = document.createElement('label');
    label2.innerHTML = ' (Sobe : ' + apartman.brojSoba + ')';
    div.appendChild(label2);

    var br2 = document.createElement('br');
    div.appendChild(br2);

    var label3 = document.createElement('label');
    label3.innerHTML = apartman.location.adresa.ulica + 
                        " " + 
                        apartman.location.adresa.broj + 
                        ", " +
                        apartman.location.adresa.naseljenoMesto +
                        ", " + 
                        apartman.location.adresa.postanskiBroj;
    div.appendChild(label3);

    var br3 = document.createElement('br');
    div.appendChild(br3);

    var label4 = document.createElement('label');
    label4.innerHTML = "prise : " + apartman.cenaPoNoci + "&euro;";
    div.appendChild(label4);

    var br4 = document.createElement('br');
    div.appendChild(br4);

    var label5 = document.createElement('label');
    label5.innerHTML = "host : " +  apartman.usernameHost;
    div.appendChild(label5);

    var br5 = document.createElement('br');
    div.appendChild(br5);

    if (loggedClient != "empty"){
        if (loggedClient.role === "administrator" || loggedClient.role === "host"){
            var button = document.createElement('button');
            button.innerHTML = "see more...";
            button.id = apartman.apartmanId;
            button.addEventListener('click', function(){
                for (var i = 0; i < apartmans.length; i++){
                    if (apartmans[i].apartmanId == this.id){
                        $.post("http://localhost:3413/api/SeeApartman",apartmans[i],
                            function(data,status){
                                if (status == "success"){
                                    location.replace("http://localhost:3413/html/see apartman.html");
                                }
                            });
                    }
                }
            })
            div.appendChild(button);

            var buttonComment = document.createElement('button');
            buttonComment.innerHTML = "comments";
            buttonComment.id = apartman.apartmanId;
            buttonComment.addEventListener('click', function(){
                for (var i = 0; i < apartmans.length; i++){
                    if (apartmans[i].apartmanId == this.id){
                        $.post("http://localhost:3413/api/SeeApartman",apartmans[i],
                            function(data,status){
                                if (status == "success"){
                                    location.replace("http://localhost:3413/html/komentari.html");
                                }
                            });
                    }
                }
            })
            div.appendChild(buttonComment);
        }
        else{ // gest (vide komentare i imaju stranicu za rezervaciju)
            var button = document.createElement('button');
            button.innerHTML = "rezerve";
            button.id = apartman.apartmanId;
            button.addEventListener('click', function(){
                for (var i = 0; i < apartmans.length; i++){
                    if (apartmans[i].apartmanId == this.id){
                        $.post("http://localhost:3413/api/SeeApartman",apartmans[i],
                            function(data,status){
                                if (status == "success"){
                                    location.replace("http://localhost:3413/html/rezerve.html");
                                }
                            });
                    }
                }
            })
            div.appendChild(button);

            var buttonComment = document.createElement('button');
            buttonComment.innerHTML = "comments";
            buttonComment.id = apartman.apartmanId;
            buttonComment.addEventListener('click', function(){
                for (var i = 0; i < apartmans.length; i++){
                    if (apartmans[i].apartmanId == this.id){
                        $.post("http://localhost:3413/api/SeeApartman",apartmans[i],
                            function(data,status){
                                if (status == "success"){
                                    location.replace("http://localhost:3413/html/komentari.html");
                                }
                            });
                    }
                }
            })
            div.appendChild(buttonComment);
        }
    }
    else{ // not logged (mogu samo komentare)
        var buttonComment = document.createElement('button');
        buttonComment.innerHTML = "comments";
        buttonComment.id = apartman.apartmanId;
        buttonComment.addEventListener('click', function(){
            for (var i = 0; i < apartmans.length; i++){
                if (apartmans[i].apartmanId == this.id){
                    $.post("http://localhost:3413/api/SeeApartman",apartmans[i],
                        function(data,status){
                            if (status == "success"){
                                location.replace("http://localhost:3413/html/komentari.html");
                            }
                        });
                }
            }
        })
        div.appendChild(buttonComment);
    }
    
    div_s2id01.appendChild(div);
}
//#endregion