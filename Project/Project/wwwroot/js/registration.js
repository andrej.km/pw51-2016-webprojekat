var username;
var password;
var rePassword;
var name;
var surname;
var sex;

//var adminLogged = $("#hiddenRole")[0].value;
//alert(adminLogged);

var user;

var allUsernames;
//console.log(allUsernames);
function getAllUsernames(){
    $.get("http://localhost:3413/api/Register", function(data, status){
        //console.log(data);
        allUsernames = data.usernames.split('/');
    })
}



var adminLogged;
var loggedClient = {};
$(document).ready(function(){
    $.get("http://localhost:3413/api/Logged", function(data, status){
        if (data != "empty") {
            loggedClient = JSON.parse(data);
            adminLogged = loggedClient.role;
        }
    }); 
});

$(document).ready(function(){
    
    getAllUsernames();

    $("#btnReg").click(function(){
        username = $("#username")[0].value;
        password = $("#password")[0].value;
        rePassword = $("#rePassword")[0].value;
        name = $("#name")[0].value;
        surname = $("#surname")[0].value;
        sex = $("#sex")[0].value;

        console.log(adminLogged);

        if (isValid()){
            if (adminLogged === 'administrator'){
                user = {
                    "username": username,
                    "password": password,
                    "name": name,
                    "surname": surname,
                    "sex": sex,
                    "role": "host"
                };
            }
            else{
                user = {
                    "username": username,
                    "password": password,
                    "name": name,
                    "surname": surname,
                    "sex": sex,
                    "role": "gest"
                };
            }

            sendUser(user);

            //console.log(user);
            $("#btnReg").css({"visibility":"hidden"});
            alert("Registated!");
        }
        else{

        }
    });
});


function sendUser(userToReg){
    $.ajax({  
        url: 'http://localhost:3413/api/Register',  
        type: 'POST',  
        dataType: 'json',  
        data: userToReg
    });  
}

function isValid(){
    var valid = true;
    if (username === ""){
        valid = false;
        myError("isValidID1", "fielset-username", "er-f-1", "p-username", "username can't be empty!");
    }
    else if (username != ""){
        for (i = 0; i < allUsernames.length; i++){
            if (username == allUsernames[i]){
                valid = false;
                myError("isValidID1", "fielset-username", "er-f-1", "p-username", "username already exists!");
                break;
            }
        }
        for (i = 0; i < username.length; i++){ // proverava da li postoji razmak
            if (username[i] === " "){
                valid = false;
                myError("isValidID1", "fielset-username", "er-f-1", "p-username", "username can't have spaces!");
                break;
            }
        }
        if (valid){
            noError("isValidID1", "fielset-username", "er-f-1", "p-username");
        }
    }
    else{
        noError("isValidID1", "fielset-username", "er-f-1", "p-username");
    }

    if (password === ""){
        valid = false;
        myError("isValidID2", "fielset-password", "er-f-2", "p-password", "password can't be empty!");
    }
    else{
        noError("isValidID2", "fielset-password", "er-f-2", "p-password");
    }

    if (rePassword === ""){
        valid = false;
        myError("isValidID3", "fielset-rePassword", "er-f-3", "p-rePassword", "reantered password can't be empty!");
    }
    else{
        noError("isValidID3", "fielset-rePassword", "er-f-3", "p-rePassword");
    }

    if (password != rePassword){
        valid = false;
        myError("isValidID3", "fielset-rePassword", "er-f-3", "p-rePassword", "password and reantered password must be the same!");
    }
    else{
        noError("isValidID3", "fielset-rePassword", "er-f-3", "p-rePassword");
        if (rePassword === ""){
            valid = false;
            myError("isValidID3", "fielset-rePassword", "er-f-3", "p-rePassword", "reantered password can't be empty!");
        }
        else{
            noError("isValidID3", "fielset-rePassword", "er-f-3", "p-rePassword");
        }
    }

    if (name === ""){
        valid = false;
        myError("isValidID4", "fielset-name", "er-f-4", "p-name", "name can't be empty!");
    }
    else{
        if (!isLetter(name[0])){
            valid = false;
            myError("isValidID4", "fielset-name", "er-f-4", "p-name", "name must start with a letter!");
        }
        else{
            noError("isValidID4", "fielset-name", "er-f-4", "p-name");
        }   
    }

    if (surname === ""){
        valid = false;
        myError("isValidID5", "fielset-surname", "er-f-5", "p-surname", "surname can't be empty!");
    }
    else{
        if (!isLetter(surname[0])){
            valid = false;
            myError("isValidID5", "fielset-surname", "er-f-5", "p-surname", "surname must start with a letter!");
        }
        else{
            noError("isValidID5", "fielset-surname", "er-f-5", "p-surname");
        }  
    }

    if (sex === ""){
        valid = false;
        myError("isValidID6", "fielset-sex", "er-f-6", "p-sex", "You must choose a gender!");
    }
    else{
        noError("isValidID6", "fielset-sex", "er-f-6", "p-sex");
    }

    if (!valid){
        $("#btnReg").css({"color":"red", "border-color":"red"});
    }
    else{
        $("#btnBack").css({"float":"right"});
        $("#btnReg").css({"float":"left"});
    }

    return valid;
};

function noError(isValidID, fields, er, pp){
    $("#" + isValidID + "").css({"visibility":"visible","display":"block", "color": "green"});
    $("#" + fields + "").css({"border-color":"green"});
    $("#" + er + "").css({"visibility":"hidden", "margin-bottom": "-4ch"});
    $("#" + pp + "")[0].innerHTML = "";
}

function myError(isValidID, fields, er, pp, message){
    $("#" + isValidID + "").css({"visibility":"visible","display":"block", "color": "red"});
    $("#" + fields + "").css({"border-color":"red"});
    $("#" + er + "").css({"visibility":"visible", "margin-bottom": "0"});
    $("#" + pp + "")[0].innerHTML = message;
}

// f-ja koja proverava da li je prvi karakter slovo 
function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
};

