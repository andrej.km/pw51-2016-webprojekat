var username;
var password;

var user;

var serviceRetVal;

$(document).ready(function(){
    //getAllUsers(); // ne radi

    $("#btnReg").click(function(){
        username = $("#username")[0].value;
        password = $("#password")[0].value;
        

        if (isValid()){
            
            user = {
                "username": username,
                "password": password
            };

            sendUser(user);
            
        }
    });
});

function sendUser(userToLog){
    request = $.ajax({  
        url: 'http://localhost:3413/api/LogInOut',  
        type: 'POST',  
        dataType: 'json',  
        data: userToLog
    });  
    
    request.done(function (response, textStatus, jqXHR){
        serviceRetVal = response;
        if (serviceRetVal === "success"){
            $("#btnBack").css({"float":"right"});
            $("#btnReg").css({"float":"left"});
            $("#btnReg").css({"visibility":"hidden"});
            alert("Logged!");
        }
        else{
            $("#btnReg").css({"color":"red", "border-color":"red"});
            myError("isValidID1", "fielset-username", "er-f-1", "p-username", "username not found!");
            myError("isValidID2", "fielset-password", "er-f-2", "p-password", "password not found!");
            alert('Unknown username');
        }   
    })
}

function isValid(){
    var valid = true;
    if (username === ""){
        valid = false;
        myError("isValidID1", "fielset-username", "er-f-1", "p-username", "username can't be empty!");
    }
    else if (username != ""){
        for (i = 0; i < username.length; i++){
            if (username[i] === " "){
                valid = false;
                myError("isValidID1", "fielset-username", "er-f-1", "p-username", "username can't have spaces!");
                break;
            }
            else{
                noError("isValidID1", "fielset-username", "er-f-1", "p-username");
            }
        }
    }
    else{
        noError("isValidID1", "fielset-username", "er-f-1", "p-username");
    }

    if (password === ""){
        valid = false;
        myError("isValidID2", "fielset-password", "er-f-2", "p-password", "password can't be empty!");
    }
    else{
        noError("isValidID2", "fielset-password", "er-f-2", "p-password");
    }
    

    return valid;
};

function noError(isValidID, fields, er, pp){
    $("#" + isValidID + "").css({"visibility":"visible","display":"block", "color": "green"});
    $("#" + fields + "").css({"border-color":"green"});
    $("#" + er + "").css({"visibility":"hidden", "margin-bottom": "-4ch"});
    $("#" + pp + "")[0].innerHTML = "";
}

function myError(isValidID, fields, er, pp, message){
    $("#" + isValidID + "").css({"visibility":"visible","display":"block", "color": "red"});
    $("#" + fields + "").css({"border-color":"red"});
    $("#" + er + "").css({"visibility":"visible", "margin-bottom": "0"});
    $("#" + pp + "")[0].innerHTML = message;
}

// f-ja koja proverava da li je prvi karakter slovo 
function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
};

