var apartman;
var amenitieIndex;

var rezervacija = new Object;
var loggedClient;

var stop = false;

$(document).ready(function () {
    //alert();
    $.get("http://localhost:3413/api/SeeApartman", function(data, status){
        apartman = JSON.parse(data);
        //console.log(apartman);
        ispisPodataka(apartman);
    }); 

    $.get("http://localhost:3413/api/Logged", function(data, status){
        loggedClient = JSON.parse(data);
    });

    $("#btn-rezerve").click(function(){
        var datumPostoji = false;

        rezervacija.apartmanId = apartman.apartmanId;
        rezervacija.usernameGest = loggedClient.username;

        var divDatumi1 = document.getElementById('datumiZaIzdavanje');
        var sviDatumi = divDatumi1.getElementsByTagName("button");
        var inputsZaDatume = divDatumi1.getElementsByTagName("input");
        var dates = "";
        var ukupnaCena = 0;
        var brojNocenja = 0;
        for (let t = 0; t < sviDatumi.length; t++){
            if (sviDatumi[t].style.color === 'green'){
                //console.log("pre dodavanja " + t + " - " + sviDatumi[t].innerHTML);
                var temppp;
                if (dates != ""){
                    temppp = dates.split(',');
                    //console.log(temppp);
                    var NASAOJE = false;
                    for (let aabb = 0 ; aabb < temppp.length - 1; aabb++){
                        if (temppp[aabb] == sviDatumi[t].innerHTML){
                            NASAOJE = true;
                            break;
                        }
                    }
                    if (NASAOJE == true){
                        continue;
                    }
                }
                //console.log(dates);
                dates += sviDatumi[t].innerHTML + ",";
                //console.log(dates);
                //console.log("posle dodavanja " + t + " - " + dates);
                var tempInput = document.getElementById(sviDatumi[t].innerHTML + '_input').value;
                if (tempInput == ""){
                    tempInput = 1;
                }
                
                var datumiKojeKorisnikZeli = [];
                for (let j = 1; j < tempInput; j++){
                    var datumSplit = sviDatumi[t].innerHTML.split('-');
                    var tempDate = parseInt(datumSplit[0]) + j + "-" + datumSplit[1] + "-" + datumSplit[2];
                    // dorada datuma da bude u formatu dd-mm-yyyy
                    var sredjenDatum = srediDatum(tempDate, tempInput);
                    //console.log(sredjenDatum);
                    var datumiApartmana = apartman.datumiZaIzdavanje.split(',');
                    //console.log (datumiApartmana);
                    for (let h = 0 ; h < datumiApartmana.length; h++){
                        datumPostoji = false;
                        //console.log (datumiApartmana[h]);
                        //console.log (sredjenDatum);
                        //console.log ("------------");
                        if (datumiApartmana[h] === sredjenDatum){
                            datumPostoji = true;
                            datumiKojeKorisnikZeli.push(sredjenDatum);
                            break;
                        }
                    }
                }

                //console.log(datumiKojeKorisnikZeli + " : " + datumiKojeKorisnikZeli.length);
                //console.log(tempInput);
                if (datumiKojeKorisnikZeli.length + 1 == tempInput){ 
                    stop = false;
                    for (let p = 0; p < datumiKojeKorisnikZeli.length; p++){
                        var tempdatumi = dates.split(',');
                        var postosjiDatum = false;
                        for (let aa = 0; aa < tempdatumi.length - 1; aa++){
                            //console.log(tempdatumi);
                            //console.log(datumiKojeKorisnikZeli[p]);
                            if (datumiKojeKorisnikZeli[p] == tempdatumi[aa]){
                                postosjiDatum = true;
                                break;
                            }
                        }
                        if (postosjiDatum == false){
                            dates += datumiKojeKorisnikZeli[p] + ",";
                            brojNocenja++;
                            //console.log(datumiKojeKorisnikZeli[p]);
                            if (proveriDaLiJeVikendIliNeradanDan(datumiKojeKorisnikZeli[p]) === 10){
                                ukupnaCena -= apartman.cenaPoNoci / 10; // ako je vikend smanji se za 10%
                            }
                            else if (proveriDaLiJeVikendIliNeradanDan(datumiKojeKorisnikZeli[p]) === 5){
                                ukupnaCena += apartman.cenaPoNoci / 20; // ako je neradni dan povecaj za 5%
                            }
                            else{
                                ukupnaCena += apartman.cenaPoNoci;
                            }
                        }
                    }
                }
                else{
                    stop = true;
                }

                
                brojNocenja++;
                ukupnaCena += apartman.cenaPoNoci;

                if (proveriDaLiJeVikendIliNeradanDan(sviDatumi[t].innerHTML) === 10){
                    ukupnaCena -= apartman.cenaPoNoci / 10; // ako je vikend smanji se za 10%
                }
                else if (proveriDaLiJeVikendIliNeradanDan(sviDatumi[t].innerHTML) === 5){
                    ukupnaCena += apartman.cenaPoNoci / 20; // ako je neradni dan povecaj za 5%
                }
            }
        }
        dates = dates.slice(0, dates.length - 1);
        rezervacija.dateTimeStart = dates;

        rezervacija.brojNocenja = brojNocenja;
        rezervacija.ukupnaCena = ukupnaCena;
        
        rezervacija.status = 0;

        if (ukupnaCena === 0 || stop === true){
            alert("Date error!")
        }
        else{
            apartman.rezervacije.push(rezervacija);
            //console.log(apartman);
            sendApartman(apartman);
            alert("Rezervation is created!");
            $("#btn-rezerve").hide();
        }
    });
});

function ispisPodataka(apartman){
    divAmenities = document.getElementById("amenities");
    labelA = document.createElement('label');
    labelA.innerHTML = "Amenities : ";
    divAmenities.appendChild(labelA);

    var bre1 = document.createElement("br");
    divAmenities.appendChild(bre1);
    
    for (let am = 0 ; am < apartman.amenities.length; am++){
        labelAm = document.createElement('label');
        labelAm.innerHTML = "-" + apartman.amenities[am].naziv;
        divAmenities.appendChild(labelAm);
        var bre12 = document.createElement("br");
        divAmenities.appendChild(bre12);
    }

    divBrojGostiju = document.getElementById("brojGostiju");
    labelBrojGostiju = document.createElement('label');
    labelBrojGostiju.innerHTML = "Broj gostiju : " + apartman.brojGostiju;
    divBrojGostiju.appendChild(labelBrojGostiju);

    divBrojSoba = document.getElementById("brojSoba");
    labelBrojSoba = document.createElement('label');
    labelBrojSoba.innerHTML = "Broj soba : " + apartman.brojSoba;
    divBrojSoba.appendChild(labelBrojSoba);

    divCenaPoNoci = document.getElementById("cenaPoNoci");
    labelCenaPoNoci = document.createElement('label');
    labelCenaPoNoci.innerHTML = "Cena po noci : " + apartman.cenaPoNoci + "&euro;";
    divCenaPoNoci.appendChild(labelCenaPoNoci);

    var getRezervacija = apartman.rezervacije;
    var rezervisaniDatumi = [];
    if (getRezervacija != 0){
        for (let l = 0; l < getRezervacija.length; l++){
            rezervisaniDatumi.push(getRezervacija[l].dateTimeStart.split(','));
        }
    }

    // slobodni datumi
    var slobodniDatumi = [];
    var sviDatumi = apartman.datumiZaIzdavanje.split(',');
    for (let b3 = 0; b3 < sviDatumi.length; b3++){
        var postoji = false;
        for (let b1 = 0; b1 < rezervisaniDatumi.length; b1++){
            for (let b2 = 0; b2 < rezervisaniDatumi[b1].length; b2++){
                var temp = rezervisaniDatumi[b1][b2];
                if (temp == sviDatumi[b3]){
                    postoji = true;
                    break;
                }
            }
        }
        if (postoji == false){
            slobodniDatumi.push(sviDatumi[b3]);
        }
    }

    // sakrij dugme ako nema datuma
    var dugmeZaRegistraciju = document.getElementById("btn-rezerve");
    if (slobodniDatumi == 0){
        dugmeZaRegistraciju.style.display = "none";
    }
    

    divDatumiZaIzdavanje = document.getElementById("datumiZaIzdavanje");
    labelDatumiZaIzdavanje = document.createElement('label');
    labelDatumiZaIzdavanje.innerHTML = "Datumi : ";
    divDatumiZaIzdavanje.appendChild(labelDatumiZaIzdavanje);
    var datumi = slobodniDatumi;
    //console.log(datumi);
    for (let i = 0; i < datumi.length; i++){
        var buttonDatumiZaIzdavanje = document.createElement('button');
        buttonDatumiZaIzdavanje.innerHTML = datumi[i];
        buttonDatumiZaIzdavanje.id = 'btn_' + datumi[i];
        buttonDatumiZaIzdavanje.style.color = 'black';
        buttonDatumiZaIzdavanje.addEventListener('click', function(){
            if (this.style.color == 'green'){
                this.style.color = 'black';
                var input = document.getElementById(this.id.split("_")[1] + "_input");
                divDatumiZaIzdavanje.removeChild(input);
            }
            else{
                this.style.color = 'green';
                var input = document.createElement('input');
                input.id = datumi[i] + "_input";
                input.type = "number";
                input.min = "1";
                input.min = "100";
                input.placeholder = "num of days";
                divDatumiZaIzdavanje.appendChild(input);
            }
        });
        divDatumiZaIzdavanje.appendChild(buttonDatumiZaIzdavanje);
    }

    divEndHour = document.getElementById("endHour");
    labelEndHour = document.createElement('label');
    labelEndHour.innerHTML = "Odjavljivanje : " + apartman.endHour.hour + apartman.endHour.hourType;
    divEndHour.appendChild(labelEndHour);

    divLocation = document.getElementById("location");
    labelLocation = document.createElement('label');
    labelLocation.innerHTML = "Location : " + apartman.location.adresa.ulica + ", " + apartman.location.adresa.broj + ", " + apartman.location.adresa.naseljenoMesto + ", " + apartman.location.adresa.postanskiBroj;
    divLocation.appendChild(labelLocation);

    divSlike = document.getElementById("slike");
    labelSlike = document.createElement('label');
    labelSlike.innerHTML = "Slike : ";
    divSlike.appendChild(labelSlike);
    var br = document.createElement('br');
    divSlike.appendChild(br);
    for (let i = 0; i < apartman.slike.length; i++){
        var img = document.createElement("img");
        img.src = apartman.slike[i].imgSrc;
        img.width = '400';
        img.height = '300';
        img.style.margin = '5';
        divSlike.appendChild(img);
    }

    divStartHour = document.getElementById("startHour");
    labelStartHour = document.createElement('label');
    labelStartHour.innerHTML = "Prijavljivanje : " + apartman.startHour.hour + apartman.startHour.hourType;
    divStartHour.appendChild(labelStartHour);

    divType = document.getElementById("type");
    labelType = document.createElement('label');
    labelType.innerHTML = "Type : " + apartman.type;
    divType.appendChild(labelType);

    divUsernameHost = document.getElementById("usernameHost");
    labelUsernameHost = document.createElement('label');
    labelUsernameHost.innerHTML = "Host : " + apartman.usernameHost;
    divUsernameHost.appendChild(labelUsernameHost);
}

function sendApartman(apartman){
    $.ajax({  
        url: 'http://localhost:3413/api/SaveChangedApartman',  
        type: 'POST',  
        dataType: 'json',  
        data: apartman
    });  
}

function proveriDaLiJeVikendIliNeradanDan(date){
    lista = ["1-1-2019", "2-1-2019", "7-1-2019", "25-12-2019",
             "15-2-2019", "16-2-2019", "19-4-2019", "20-4-2019", 
             "21-4-2019", "22-4-2019", "26-4-2019", "27-4-2016",
             "28-4-2019", "29-4-2019", "1-5-2019", "2-5-2019",
             "4-6-2019", "11-8-2019", "18-8-2019", "11-11-2019"];
    for (let i = 0; i < lista.length; i++){
        if (date === lista[i]){
            return 5;  // to je 5%
        }
    }
    var deloviDatuma = date.split('-');
    var d = new Date(deloviDatuma[2] + "-" + deloviDatuma[1] + "-" + deloviDatuma[0]);

    if (d.getDay() === 0 || d.getDay() === 5 || d.getDay() === 6){
        return 10;
    }
    else {
        return 0;
    }
}

function srediDatum(datum, brojDana){
    //console.log("primio : " + datum)

    var temp = datum.split('-');

    for (let g = 0; g < brojDana; g++){
        if (parseInt(temp[1]) === 4 || parseInt(temp[1]) === 6 || parseInt(temp[1]) === 9 
        || parseInt(temp[1]) === 11){   // parni meseci
    
            if (parseInt(temp[0]) > 30){
                temp[0] = parseInt(temp[0]) - 30;
                temp[1] = parseInt(temp[1]) + 1; // povacaj mesec
            }
        }
    
        if (parseInt(temp[1]) === 1 || parseInt(temp[1]) === 3 || parseInt(temp[1]) === 5 
        || parseInt(temp[1]) === 7 || parseInt(temp[1]) === 8 || parseInt(temp[1]) === 10 
        || parseInt(temp[1]) === 12){   // neparni meseci
            //console.log(temp[0]);
            if (parseInt(temp[0]) > 31){
                //console.log("usao");
                temp[0] = parseInt(temp[0]) - 31;
                temp[1] = parseInt(temp[1]) + 1; // povacaj mesec
                if (parseInt(temp[1]) === 13){
                    temp[0] = "1";
                    temp[1] = "1";
                    temp[2] = parseInt(temp[2]) + 1;   //nova godina
                }
            }
        }
    
        if (parseInt(temp[1]) === 2){   // februar bez prestupne godine
    
            if (parseInt(temp[0]) > 28){
                temp[0] = parseInt(temp[0]) - 28;
                temp[1] = parseInt(temp[1]) + 1; // povacaj mesec
            }
        }
    }

    

    if (parseInt(temp[0]) < 10){   // dodavanje 0 na dan
        temp[0] = "0" + parseInt(temp[0]);
    }

    if (parseInt(temp[1]) < 10){  // dodavanje 0 na mesec
        temp[1] = "0" + parseInt(temp[1]);
    }

    return temp[0] + "-" + temp[1] + "-" + temp[2];
}