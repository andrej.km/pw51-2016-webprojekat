$(document).ready(function(){
  // uzmi sve klijente
  var allClients = $.get("http://localhost:3413/api/AllClients", function(data, status){
    allClients = data.split('\n');
    for (let i = 0; i < allClients.length - 1; i++) {
      allClients[i] = JSON.parse(allClients[i]);
      document.getElementById('all-clients').appendChild(makeDivs(allClients[i], i));
    }
  }); 
})

function makeDivs(array, index) {
  var br = document.createElement('br');
  var br1 = document.createElement('br');
  var br2 = document.createElement('br');
  var br3 = document.createElement('br');
  var hr = document.createElement('hr');
  
  // Create a div
  var div = document.createElement('div');
  div.id = "user" + index;

  // Kreirao sam prvo sa formom i kad  sam pokusao da je izbacim,
  // nije htelo da radi, pa sam je ostavio a inace se ne koristi
  // nego samo tako drzi 3 elementa
  // Create a form
  var form = document.createElement('form');
  
  var submit = document.createElement('button'); // submit dugme za formu
  submit.className = 'submit-btn';
  
  submit.addEventListener('click', function(){
    user = {
      username : array.username,
      password : ""
    }
    $.ajax({  
      url: 'http://localhost:3413/api/AllClients',  
      type: 'POST',  
      dataType: 'json',  
      data: user
    });  
    location.reload();
  });
  var labelBlocked = document.createElement('label');
  labelBlocked.innerHTML = "Blocked";
  var blocked = document.createElement('label'); // blocked
  blocked.id = 'lBlocked';
  blocked.appendChild(document.createTextNode(array.blocked));
  if(blocked.innerHTML === "true"){
    submit.innerHTML = "Unblock";
  }
  else{
    submit.innerHTML = "Block";
  }

  form.appendChild(labelBlocked);
  form.appendChild(blocked);
  form.appendChild(submit);
  
  var labelUsername = document.createElement('label');
  labelUsername.innerHTML = "Username";
  var username = document.createElement('label'); // username
  username.id = 'lUsername';
  username.appendChild(document.createTextNode(array.username));

  var labelName = document.createElement('label');
  labelName.innerHTML = "Name";
  var name = document.createElement('label'); // name
  name.id = 'lName';
  name.appendChild(document.createTextNode(array.name));
  
  var labelSurname = document.createElement('label');
  labelSurname.innerHTML = "Surname";
  var surname = document.createElement('label'); // surname
  surname.id = 'lSurname';
  surname.appendChild(document.createTextNode(array.surname));

  var labelGender = document.createElement('label');
  labelGender.innerHTML = "Gender";
  var gender = document.createElement('label'); // gender
  gender.id = 'lGender';
  gender.appendChild(document.createTextNode(array.sex));

  var labelRole = document.createElement('label');
  labelRole.innerHTML = "Role";
  var role = document.createElement('label'); // role
  role.id = 'lRole';
  role.appendChild(document.createTextNode(array.role));
  
  div.appendChild(labelUsername);
  div.appendChild(username);
  div.appendChild(br);

  div.appendChild(labelName);
  div.appendChild(name);
  div.appendChild(br1);

  div.appendChild(labelSurname);
  div.appendChild(surname);
  div.appendChild(br2);

  div.appendChild(labelGender);
  div.appendChild(gender);
  div.appendChild(br3);

  div.appendChild(labelRole);
  div.appendChild(role);

  div.appendChild(form);

  var buttonForChange = document.createElement('button');
  buttonForChange.style.height = '40px';
  buttonForChange.style.borderRadius = '4px';
  buttonForChange.style.fontSize = '3ch';
  buttonForChange.style.width = '10ch';
  buttonForChange.id = 'bc_' + array.username;
  buttonForChange.innerHTML = "Change";
  
  buttonForChange.addEventListener('click', function(){
    this.style.visibility = "hidden";
    var username = this.id.split('_')[1]; // username od korisnika za kojeg se menjaju podaci
    var div = document.getElementById('dc_' + username);

    var name = document.createElement('input');
    name.placeholder = 'Name';
    name.id = 'nc_' + username;
    div.appendChild(name);
    var br1 = document.createElement('br');
    div.appendChild(br1);

    var surname = document.createElement('input');
    surname.placeholder = 'Surname';
    surname.id = 'sc_' + username;
    div.appendChild(surname);
    var br2 = document.createElement('br');
    div.appendChild(br2);

    var gender = document.createElement('select');
    gender.id = 'gc_' + username;
    // stilove nije hteo da cita u .css-u pa sam rucno dodao ovde!
    gender.style.height = '40px';
    gender.style.borderRadius = '4px';
    gender.style.fontSize = '3ch';
    gender.style.width = '10ch';
    var male = document.createElement("option");
    male.text = "Male";
    gender.options.add(male, 1);
    var female = document.createElement("option");
    female.text = 'Female';
    gender.options.add(female, 2);
    div.appendChild(gender);
    var br3 = document.createElement('br');
    div.appendChild(br3);

    var buttonSendChangedData = document.createElement('button');
    buttonSendChangedData.style.height = '40px';
    buttonSendChangedData.style.borderRadius = '4px';
    buttonSendChangedData.style.fontSize = '3ch';
    buttonSendChangedData.style.width = '10ch';
    buttonSendChangedData.innerHTML = 'Send';
    buttonSendChangedData.id = 'bscd_' + username;
    buttonSendChangedData.addEventListener('click', function(){
      // uzmi podatke
      var name = document.getElementById('nc_' + username).value;
      var surname = document.getElementById('sc_' + username).value;
      var gender = document.getElementById('gc_' + username).value;
      var sendChangedUser = {
        'username' : username,
        'password' : 'admin', // password samo sluzi da bi kontroler znao da admin menja podatke i da ne zeli da se njegovi podaci promene u logged.txt
        'name' : name, 
        'surname' : surname,
        'sex' : gender
      };
      $.ajax({  
        url: 'http://localhost:3413/api/Logged',   // koristim isti kontorler kao i za menjanje podataka ulogovanog
        type: 'POST',  
        dataType: 'json',  
        data: sendChangedUser
      });  
      location.reload();
    });
    div.appendChild(buttonSendChangedData);
  });
  div.appendChild(buttonForChange);

  var divForChange = document.createElement('div');
  divForChange.id = 'dc_' + array.username;
  div.appendChild(divForChange);

  div.appendChild(hr);
  
  return div;
}
