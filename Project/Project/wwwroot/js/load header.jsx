$(document).ready(function(){
    getLogged();
});


function getLogged(){
    $.get("http://localhost:3413/api/Logged", function(data, status){
        if(data != "empty"){
            var d = JSON.parse(data);
            ReactDOM.render(
                <ul>
                    <li><a id="users-data" href="html/users data.html">{d.username}</a></li>
                    <li><a id="log-out" href="javascript:logOutUser();">Log out</a></li>
                </ul>,
                document.getElementById('logged-header')
            );
            $("#hiddenRole")[0].value = d.role; // ovde upisujemo koja je uloga ulogovanog da ne bi stavlo isli na server
            $('#log-out').css("cursor", "pointer");
        }
        else{
            ReactDOM.render(
                <ul>
                    <li><a id="sign-up" href="html/registration.html">Sign up</a></li>
                    <li><a id="log-in"  href="html/log in.html">Log in</a></li>
                </ul>,
                document.getElementById('logged-header')
            );
        }
      });
}

function logOutUser(){
    $.get("http://localhost:3413/api/LogInOut");
    ReactDOM.render(
        <ul>
            <li><a id="sign-up" href="html/registration.html">Sign up</a></li>
            <li><a id="log-in"  href="html/log in.html">Log in</a></li>
        </ul>,
        document.getElementById('logged-header')
    );
    location.reload();
}