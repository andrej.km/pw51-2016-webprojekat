var loggedClient = {
    "username": "",
    "password": "",
    "name": "",
    "surname": "",
    "sex": "",
    "role": ""
};
// uzima podatke od ulogovanog klijenta
$(document).ready(function () {
    $.get("http://localhost:3413/api/Logged", function(data, status){
        if (data != "empty") {
            loggedClient = JSON.parse(data);
            ispisPodataka(loggedClient);
            
        }
    }); 
});

function ispisPodataka(loggedClient) {
    console.log(loggedClient);
    new Vue({
        el: '#username',
        data: { username: loggedClient.username }
    })

    new Vue({
        el: '#name',
        data: { name: loggedClient.name }
    })

    new Vue({
        el: '#surname',
        data: { surname: loggedClient.surname }
    })

    new Vue({
        el: '#password',
        data: { password: loggedClient.password }
    })

    new Vue({
        el: '#gender',
        data: {
            selected: loggedClient.sex
        }
    })

    new Vue({
        el: '#role',
        data: { role: loggedClient.role }
    })
}

// prilikom crtanja tela, da se sakrije
$(document).ready(function (){
    $("#pName").toggle();
    $("#pSurname").toggle();
    $("#pPassword").toggle();
    $("#pGender").toggle();
});
