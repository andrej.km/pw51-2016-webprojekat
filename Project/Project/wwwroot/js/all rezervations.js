var loggedClient = {};
var apartmans = [];
var allApartmans = [];
var odradioUcitavenjeUlogovanog = false;
var odradioGetAllApartmans = false;

$(document).ready(function(){
    $.get("http://localhost:3413/api/Logged", function(data, status){
        if (odradioUcitavenjeUlogovanog == false){
            if (data != "empty") {
                loggedClient = JSON.parse(data);
                //console.log(loggedClient.role);
                odradioUcitavenjeUlogovanog = true;
            }
        }
    });

    $.get("http://localhost:3413/api/AllApartmans", function(data, status){
        if (odradioGetAllApartmans == false){
            odradioGetAllApartmans = true;
            allApartmans = data.split('\n');
            allApartmans.pop(); // da sklonimo prazan 
            for (var i = 0; i < allApartmans.length; i++){
                apartmans.push(JSON.parse(allApartmans[i]));
                var tempApartman = JSON.parse(allApartmans[i]);
                
                if (loggedClient.role === "administrator"){
                    show(tempApartman);
                }
                else if (loggedClient.role === "host"){
                    if (tempApartman.usernameHost == loggedClient.username){
                        show(tempApartman);
                    }
                }
                else if (loggedClient.role === "gest"){
                    showGest(tempApartman);
                }
            }
        }
    });
    //console.log(apartmans);
});

function show(apartman){
    var div = document.getElementById("rezervations");

    var divRezervation = document.createElement('div');
    divRezervation.style.border = "thin solid black";
    divRezervation.style.marginBottom = '5px';
    
    var hostLabel = document.createElement('label');
    hostLabel.innerHTML = "Host : " + apartman.usernameHost;
    divRezervation.appendChild(hostLabel);
    var br = document.createElement('br');
    divRezervation.appendChild(br);

    if (apartman.rezervacije != null){
        for (let i = 0; i < apartman.rezervacije.length; i++){
            var gestLabel = document.createElement('label');
            gestLabel.innerHTML = "Gest : " + apartman.rezervacije[i].usernameGest;
            divRezervation.appendChild(gestLabel);

            var br = document.createElement('br');
            divRezervation.appendChild(br);

            var divTemp = document.createElement('div');
            divTemp.style.border = "thin solid black";
            divTemp.style.margin = '5px';
            divRezervation.appendChild(divTemp);

            var label1 = document.createElement('label');
            label1.innerHTML = "Cena : " + apartman.rezervacije[i].ukupnaCena;
            divTemp.appendChild(label1);

            var br1 = document.createElement('br');
            divTemp.appendChild(br1);

            var label2 = document.createElement('label');
            label2.innerHTML = "Datumi : " + apartman.rezervacije[i].dateTimeStart;
            divTemp.appendChild(label2);

            var br2 = document.createElement('br');
            divTemp.appendChild(br2);

            var statusi = ["Kreirana", "Odbijena", "Odustanak", "Prihvacena", "Zavrsena"];

            var label3 = document.createElement('label');
            label3.innerHTML = "Status : " + statusi[apartman.rezervacije[i].status];
            divTemp.appendChild(label3);

            var br3 = document.createElement('br');
            divTemp.appendChild(br3);

            if (apartman.rezervacije[i].status == 0 && (loggedClient.role == "administrator" || loggedClient.role == "host")){
                var select = document.createElement('select');
                select.id = i;
                var option1 = document.createElement("option");
    
                option1.setAttribute("value", "1");
                var t1 = document.createTextNode("Odbij");
                option1.appendChild(t1);
    
                var option2 = document.createElement("option");
                option2.setAttribute("value", "3");
                var t2 = document.createTextNode("Prihvati");
                option2.appendChild(t2);
    
                var option3 = document.createElement("option");
                option3.setAttribute("value", "4");
                var t3 = document.createTextNode("Zavrsi");
                option3.appendChild(t3);
                
                select.appendChild(option1);
                select.appendChild(option2);
                select.appendChild(option3);
                divTemp.appendChild(select);

                var buttonSend = document.createElement('button');
                buttonSend.id = apartman.apartmanId + "_" + i;   // id je broj apartmana + redni broj rezervacije
                buttonSend.innerHTML = "Posalji";
                buttonSend.addEventListener('click', function(){
                    var id = this.id.split('_');
                    apartmans[id[0] - 1].rezervacije[id[1]].status = document.getElementById(id[1]).value;
                    //alert();
                    //console.log(document.getElementById(id[1]).value);
                    sendApartman(apartmans[id[0] - 1]);
                });
                divTemp.appendChild(buttonSend);
            }
            else if (apartman.rezervacije[i].status == 3){
                var select = document.createElement('select');
                select.id = i;
                var option1 = document.createElement("option");
    
                option1.setAttribute("value", "1");
                var t1 = document.createTextNode("Odbij");
                option1.appendChild(t1);
    
                var option3 = document.createElement("option");
                option3.setAttribute("value", "4");
                var t3 = document.createTextNode("Zavrsi");
                option3.appendChild(t3);
                
                select.appendChild(option1);
                select.appendChild(option3);
                divTemp.appendChild(select);

                var buttonSend = document.createElement('button');
                buttonSend.id = apartman.apartmanId + "_" + i;   // id je broj apartmana + redni broj rezervacije
                buttonSend.innerHTML = "Posalji";
                buttonSend.addEventListener('click', function(){
                    var id = this.id.split('_');
                    apartmans[id[0] - 1].rezervacije[id[1]].status = document.getElementById(id[1]).value;

                    sendApartman(apartmans[id[0] - 1]);

                    alert("Stanje sacuvano!");
                    location.reload();
                });
                divTemp.appendChild(buttonSend);
            }
        }
    }
    div.appendChild(divRezervation);
}

function sendApartman(apartman){
    $.ajax({  
        url: 'http://localhost:3413/api/SaveChangedApartman',  
        type: 'POST',  
        dataType: 'json',  
        data: apartman
    });  
    
    //location.reload();
}

function showGest(apartman){
    var div = document.getElementById("rezervations");

    var divRezervation = document.createElement('div');
    divRezervation.style.border = "thin solid black";
    divRezervation.style.marginBottom = '5px';
    
    var hostLabel = document.createElement('label');
    hostLabel.innerHTML = "Host : " + apartman.usernameHost;
    divRezervation.appendChild(hostLabel);
    var br = document.createElement('br');
    divRezervation.appendChild(br);

    if (apartman.rezervacije != null){
        for (let i = 0; i < apartman.rezervacije.length; i++){
            if (apartman.rezervacije[i].usernameGest == loggedClient.username){
                var gestLabel = document.createElement('label');
                gestLabel.innerHTML = "Gest : " + apartman.rezervacije[i].usernameGest;
                divRezervation.appendChild(gestLabel);
    
                var br = document.createElement('br');
                divRezervation.appendChild(br);
    
                var divTemp = document.createElement('div');
                divTemp.style.border = "thin solid black";
                divTemp.style.margin = '5px';
                divRezervation.appendChild(divTemp);
    
                var label1 = document.createElement('label');
                label1.innerHTML = "Cena : " + apartman.rezervacije[i].ukupnaCena;
                divTemp.appendChild(label1);
    
                var br1 = document.createElement('br');
                divTemp.appendChild(br1);
    
                var label2 = document.createElement('label');
                label2.innerHTML = "Datumi : " + apartman.rezervacije[i].dateTimeStart;
                divTemp.appendChild(label2);
    
                var br2 = document.createElement('br');
                divTemp.appendChild(br2);
    
                var statusi = ["Kreirana", "Odbijena", "Odustanak", "Prihvacena", "Zavrsena"];
    
                var label3 = document.createElement('label');
                label3.innerHTML = "Status : " + statusi[apartman.rezervacije[i].status];
                divTemp.appendChild(label3);

                var bra = document.createElement('br');
                divTemp.appendChild(bra);   

                if (apartman.rezervacije[i].status == 1 || apartman.rezervacije[i].status == 4){
                    var divKomentar = document.createElement('div');
                    divKomentar.style.border = "thin solid black";
                    divKomentar.style.margin = '5px';

                    var inputText = document.createElement('input');
                    inputText.type = 'text';
                    inputText.style.width = "50%";
                    inputText.id = "komentar_" + apartman.apartmanId + "_" + i;
                    inputText.placeholder = "Unesite vas komentar";
                    divKomentar.appendChild(inputText);

                    var inputOcena = document.createElement('input');
                    inputOcena.type = 'number';
                    inputOcena.id = "ocena_" + apartman.apartmanId + "_" + i;
                    inputOcena.placeholder = "Ocena";
                    inputOcena.min = "1";
                    inputOcena.max = "5";
                    inputOcena.style.width = "6%";
                    divKomentar.appendChild(inputOcena);

                    var buttonSendOcena = document.createElement('button');
                    buttonSendOcena.id = apartman.apartmanId + "_" + i;   // id je broj apartmana + redni broj rezervacije
                    buttonSendOcena.innerHTML = "Posalji";
                    buttonSendOcena.addEventListener('click', function(){
                        var id = this.id.split('_');
                        var komentar = {
                            "gestUsername" : loggedClient.username,
                            "apartmanId" : apartman.apartmanId,
                            "text" : document.getElementById("komentar_" + id[0] + "_" + id[1]).value,
                            "ocena" : document.getElementById("ocena_" + id[0] + "_" + id[1]).value,
                            "vidljiv" : false
                        };
                        //console.log(komentar);
                        if (komentar.text == "" || komentar.ocena == ""){
                            alert("Nepotpun komentar!");
                        }
                        else{
                            apartmans[id[0] - 1].komentari.push(komentar);
                        
                            //console.log(apartmans[id[0] - 1]);
                            sendApartman(apartmans[id[0] - 1]);
                            alert("Komentar sacuvan!");
                            location.reload();
                        }
                        
                    });
                    divKomentar.appendChild(buttonSendOcena);

                    
                    divTemp.appendChild(divKomentar);
                }
    
            }
            
        }
    }
    div.appendChild(divRezervation);
}