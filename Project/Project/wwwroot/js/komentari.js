var loggedClient = {};
var apartmans = [];
var allApartmans = [];
var odradioUcitavenjeUlogovanog = false;
var odradioGetAllApartmans = false;
var aparmanGEst = {};

$(document).ready(function(){
    $.get("http://localhost:3413/api/Logged", function(data, status){
        if (odradioUcitavenjeUlogovanog == false){
            if (data != "empty") {
                loggedClient = JSON.parse(data);
                //console.log(loggedClient.role);
                odradioUcitavenjeUlogovanog = true;
            }
        }
    });

    $.get("http://localhost:3413/api/SeeApartman", function(data, status){
        aparmanGEst = JSON.parse(data);
        console.log(aparmanGEst);
    }); 

    $.get("http://localhost:3413/api/AllApartmans", function(data, status){
        if (odradioGetAllApartmans == false){
            odradioGetAllApartmans = true;
            allApartmans = data.split('\n');
            allApartmans.pop(); // da sklonimo prazan 
            if (loggedClient.role === "gest"){
                showGest(aparmanGEst);
            }
            else{
                for (var i = 0; i < allApartmans.length; i++){
                    apartmans.push(JSON.parse(allApartmans[i]));
                    var tempApartman = JSON.parse(allApartmans[i]);
                    
                    if (loggedClient.role === "administrator"){
                        show(tempApartman);
                    }
                    else if (loggedClient.role === "host"){
                        if (tempApartman.usernameHost == loggedClient.username){
                            show(tempApartman);
                        }
                    }
                    
                }
            }
            
        }
    });
    console.log(apartmans);
});

function show(apartman){
    var div = document.getElementById("komentari");

    var divKomentar = document.createElement('div');
    divKomentar.style.border = "thin solid black";
    divKomentar.style.marginBottom = '5px';

    var hostLabel = document.createElement('label');
    hostLabel.innerHTML = "Host : " + apartman.usernameHost;
    divKomentar.appendChild(hostLabel);
    var br = document.createElement('br');
    divKomentar.appendChild(br);

    if (apartman.komentari != null){
        for (let i = 0; i < apartman.komentari.length; i++){
            var divTemp = document.createElement('div');
            divTemp.style.border = "thin solid black";
            divTemp.style.margin = '5px';

            var gestLabel = document.createElement('label');
            gestLabel.innerHTML = "Gest : " + apartman.komentari[i].gestUsername;
            divTemp.appendChild(gestLabel);
            var br = document.createElement('br');
            divTemp.appendChild(br);

            var text = document.createElement('label');
            text.innerHTML = "Text : " + apartman.komentari[i].text;
            divTemp.appendChild(text);
            var br1 = document.createElement('br');
            divTemp.appendChild(br1);

            var ocena = document.createElement('label');
            ocena.innerHTML = "Ocena : " + apartman.komentari[i].ocena;
            divTemp.appendChild(ocena);
            var br2 = document.createElement('br');
            divTemp.appendChild(br2);
            
            var btnVidljiv = document.createElement('button');
            if (apartman.komentari[i].vidljiv == false){
                btnVidljiv.innerHTML = "Prikazi";
            }
            else{
                btnVidljiv.innerHTML = "Sakrij";
            }
            btnVidljiv.id = apartman.apartmanId + "_" + i;
            btnVidljiv.addEventListener("click", function(){
                var id = this.id.split('_');
                if (apartmans[id[0] - 1].komentari[id[1]].vidljiv == false){
                    apartmans[id[0] - 1].komentari[id[1]].vidljiv = true;
                }
                else{
                    apartmans[id[0] - 1].komentari[id[1]].vidljiv = false;
                }
                //console.log(apartmans[id[0] - 1]);
                sendApartman(apartmans[id[0] - 1]);
                alert("Promena obavljena!");
                location.reload();
            });
            divTemp.appendChild(btnVidljiv);
        

            divKomentar.appendChild(divTemp);
        }
    }

    div.appendChild(divKomentar);
}

function showGest(apartman){
    var div = document.getElementById("komentari");

    var divKomentar = document.createElement('div');
    divKomentar.style.border = "thin solid black";
    divKomentar.style.marginBottom = '5px';

    var hostLabel = document.createElement('label');
    hostLabel.innerHTML = "Host : " + apartman.usernameHost;
    divKomentar.appendChild(hostLabel);
    var br = document.createElement('br');
    divKomentar.appendChild(br);

    if (apartman.komentari != null){
        for (let i = 0; i < apartman.komentari.length; i++){
            if (apartman.komentari[i].vidljiv == true){
                var divTemp = document.createElement('div');
                divTemp.style.border = "thin solid black";
                divTemp.style.margin = '5px';
    
                var gestLabel = document.createElement('label');
                gestLabel.innerHTML = "Gest : " + apartman.komentari[i].gestUsername;
                divTemp.appendChild(gestLabel);
                var br = document.createElement('br');
                divTemp.appendChild(br);
    
                var text = document.createElement('label');
                text.innerHTML = "Text : " + apartman.komentari[i].text;
                divTemp.appendChild(text);
                var br1 = document.createElement('br');
                divTemp.appendChild(br1);
    
                var ocena = document.createElement('label');
                ocena.innerHTML = "Ocena : " + apartman.komentari[i].ocena;
                divTemp.appendChild(ocena);
                var br2 = document.createElement('br');
                divTemp.appendChild(br2);
                
                divKomentar.appendChild(divTemp);
            }
            
        }
    }

    div.appendChild(divKomentar);
}

function sendApartman(apartman){
    $.ajax({  
        url: 'http://localhost:3413/api/SaveChangedApartman',  
        type: 'POST',  
        dataType: 'json',  
        data: apartman
    });  
    
    //location.reload();
}