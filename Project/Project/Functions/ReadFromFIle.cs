﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Functions
{
    public class ReadFromFIle : Interfaces.IReadFromFile
    {
        public string Read(string file)
        {
            string retVal = "";
            using (StreamReader sr = System.IO.File.OpenText(file))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    retVal = s;
                }
            }
            return retVal;
        }
    }
}
