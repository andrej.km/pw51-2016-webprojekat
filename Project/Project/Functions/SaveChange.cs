﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Functions
{
    public class SaveChange : Interfaces.ISaveChange
    {
        #region Fields
        private Interfaces.IReadAllFileContent readAllFileContent = new Functions.ReadAllFileContent();
        private Interfaces.IConvertToModelApartman convertToModelApartman = new Functions.ConvertToModelApartman();
        private List<Model.Apartman> apartmans = new List<Model.Apartman>();
        private Interfaces.IConvertToJSON<Model.Apartman> convertToJSON = new Functions.ConvertToJSON<Model.Apartman>();
        #endregion
        public void Save(Model.Apartman postApartman, string path)
        {
            string allApartmansStr = readAllFileContent.Read(path);
            string[] allApartmansStrArr = allApartmansStr.Split('\n');

            for (int i = 0; i < allApartmansStrArr.Length - 1; i++)  // idemo do velicina - 1 jer tako sam odradio citanje gde je posljednji prazan zbog '\n'
            {
                Model.Apartman apartman = convertToModelApartman.Convert(allApartmansStrArr[i]);

                if (apartman.apartmanId == postApartman.apartmanId)
                {
                    apartman = postApartman;
                }
                apartmans.Add(apartman);
            }
            using (StreamWriter sw = new StreamWriter(path))
            {
                foreach (var item in apartmans)
                {
                    sw.WriteLine(convertToJSON.Convert(item));
                }
            }
        }
    }
}
