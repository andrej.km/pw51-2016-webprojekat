﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Project.Model;

namespace Project.Functions
{
    public class ConvertToModelApartman : Interfaces.IConvertToModelApartman
    {
        public Apartman Convert(string jsonApartman)
         => Newtonsoft.Json.JsonConvert.DeserializeObject<Apartman>(jsonApartman);
    }
}
