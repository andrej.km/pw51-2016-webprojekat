﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Functions
{
    public class ReadAllFileContent : Interfaces.IReadAllFileContent
    {
        public string Read(string path)
        {
            string retVal = "";
            using (StreamReader sr = System.IO.File.OpenText(path))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    retVal += (s + "\n");
                }
            }
            return retVal;
        }
    }
}
