﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Functions
{
    public class ConvertToJSON<T> : Interfaces.IConvertToJSON<T>
    {
        public string Convert(T model) 
            => JsonConvert.SerializeObject(model);
    }
}
