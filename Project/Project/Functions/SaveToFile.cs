﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

/// <summary>
/// Use this class only if you need to save one json to file!!
/// </summary>
namespace Project.Functions
{
    public class SaveToFile : Interfaces.ISaveToFile
    {
        public void Save(string path, string json)
        {
            using (StreamWriter sw = System.IO.File.CreateText(path))
            {
                sw.WriteLine(json);
            }
        }
    }
}
