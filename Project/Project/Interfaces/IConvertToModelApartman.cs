﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Interfaces
{
    interface IConvertToModelApartman
    {
        Model.Apartman Convert(string jsonApartman);
    }
}
