﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Interfaces
{
    interface ISaveCreatedToFile
    {
        void Save(string path, string json);
    }
}
