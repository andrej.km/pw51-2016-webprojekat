﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.Interfaces
{
    interface IConvertToJSON<T>
    {
        string Convert(T model);
    }
}
