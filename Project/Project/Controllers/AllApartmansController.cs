﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/AllApartmans")]
    public class AllApartmansController : Controller
    {
        #region Fields
        private Interfaces.IReadAllFileContent readAllFileContent = new Functions.ReadAllFileContent();
        private readonly string path = "apartmans.txt";
        #endregion

        [HttpGet]
        public JsonResult GetAllApartmans()
            => Json(readAllFileContent.Read(path));
    }
}