﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/Register")]
    public class RegisterController : Controller
    {
        [HttpPost]
        public void RegisterClient(Model.Client client)
        {
            string jsonClient = JsonConvert.SerializeObject(client);

            string path = "clients.txt";

            

            // This text is added only once to the file.
            if (!System.IO.File.Exists(path))
            {
                // Create a file to write to.
                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine(jsonClient);
                }
            }
            else
            {
                using (StreamWriter sw = System.IO.File.AppendText(path))
                {
                    sw.WriteLine(jsonClient);
                }
            }
        }

        // ne radi
        [HttpGet]
        public JsonResult AllUsers()
        {
            string path = "clients.txt";
            var retVal = "";
            using (StreamReader sr = System.IO.File.OpenText(path))
            {
                string jsonClient = "";
                
                while ((jsonClient = sr.ReadLine()) != null)
                {
                    Model.Client client = ConvertToModel(jsonClient);
                    retVal += client.username + "/";
                }
            }
            return Json(new { usernames = retVal});
        }

        private static Model.Client ConvertToModel(string json)
            => Newtonsoft.Json.JsonConvert.DeserializeObject<Model.Client>(json);
    }
}