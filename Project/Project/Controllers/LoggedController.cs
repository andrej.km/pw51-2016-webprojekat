﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/Logged")]
    public class LoggedController : Controller
    {
        // menjanje podataka
        [HttpPost]
        public void ChangeData(Model.ChangedData newData)
        {
            string logged = ReadFromFile("logged.txt");
            Model.Client loggedClient = ConvertToModel(logged);
            
            loggedClient.name = newData.name;
            loggedClient.surname = newData.surname;
            loggedClient.sex = newData.sex;
            
            string jsonClient = JsonConvert.SerializeObject(loggedClient);
            string path = "clients.txt";
            string path2 = "logged.txt";

            // ako je password admin znaci da je admin bio ulogovan
            // i da on menja podatke i ne zelimo da se menjaju
            // podaci ulogovanog admina
            if (newData.password != "admin")
            {
                using (StreamWriter sw = System.IO.File.CreateText(path2))
                {
                    sw.WriteLine(jsonClient);
                }
            }
            

            string tempFile = Path.GetTempFileName();

            using (StreamReader sr = new StreamReader(path))
            {
                using (StreamWriter sw = new StreamWriter(tempFile))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        Model.Client clientTemp = ConvertToModel(line);
                        if (clientTemp.username == newData.username)
                        {
                            clientTemp.name = newData.name;
                            clientTemp.surname = newData.surname;
                            clientTemp.sex = newData.sex;
                            string jsonClientTemp = JsonConvert.SerializeObject(clientTemp);
                            sw.WriteLine(jsonClientTemp);
                            //break;
                        }
                        else
                        {
                            sw.WriteLine(line);
                        }
                    }
                }
            }
            System.IO.File.Delete(path);
            System.IO.File.Move(tempFile, path);
        }

        private static Model.Client ConvertToModel(string json)
            => Newtonsoft.Json.JsonConvert.DeserializeObject<Model.Client>(json);

        [HttpGet]
        public JsonResult GetLogIn()
        {
            string logged = ReadFromFile("logged.txt");

            if (logged == "")
            {
                return Json("empty");
            }

            return Json(logged);
        }

        private string ReadFromFile(string file)
        {
            string retVal = "";
            using (StreamReader sr = System.IO.File.OpenText(file))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    retVal = s;
                }
            }
            return retVal;
        }
    }
}