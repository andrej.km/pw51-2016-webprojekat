﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/AllClients")]
    public class AllClientsController : Controller
    {
        [HttpGet]
        public JsonResult GetAllClients()
        {
            string allClients = ReadFromFile("clients.txt");

            if (allClients == "")
            {
                return Json("empty");
            }

            return Json(allClients);
        }

        private string ReadFromFile(string file)
        {
            string retVal = "";
            using (StreamReader sr = System.IO.File.OpenText(file))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    retVal += (s + "\n");
                }
            }
            return retVal;
        }
        [HttpPost]
        public void BlockClient(Model.LogInClient client)
        {
            string path = "clients.txt";
            string tempFile = Path.GetTempFileName();

            using (StreamReader sr = new StreamReader(path))
            {
                using (StreamWriter sw = new StreamWriter(tempFile))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        Model.Client clientTemp = ConvertToModel(line);
                        if (clientTemp.username == client.username)
                        {
                            clientTemp.blocked = clientTemp.blocked == false ? true : false;
                            string jsonClientTemp = JsonConvert.SerializeObject(clientTemp);
                            sw.WriteLine(jsonClientTemp);
                        }
                        else
                        {
                            sw.WriteLine(line);
                        }
                    }
                }
            }
            System.IO.File.Delete(path);
            System.IO.File.Move(tempFile, path);
        }

        private static Model.Client ConvertToModel(string json)
            => Newtonsoft.Json.JsonConvert.DeserializeObject<Model.Client>(json);
    }
}