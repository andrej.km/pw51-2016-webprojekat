﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/LogInOut")]
    public class LogInOutController : Controller
    {
        private Interfaces.ISaveToFile saveToFile = new Functions.SaveToFile();
        [HttpGet]
        public void DeleteLoggedUser()
        {
            string path = "logged.txt";

            using (StreamWriter sw = System.IO.File.CreateText(path)) { }
        }

        [HttpPost]
        public string LogIn(Model.LogInClient logInClient)
        {
            string retVal = "";

            List<string> admins = ReadFromFile("admins.txt");
            List<string> clients = ReadFromFile("clients.txt");

            retVal = FindClient(admins, logInClient);
            if (retVal == "false")
            {
                retVal = FindClient(clients, logInClient);
                if (retVal == "false")
                {
                    return "failed";
                }
                else
                {
                    string path = "logged.txt";

                    using (StreamWriter sw = System.IO.File.CreateText(path))
                    {
                        sw.WriteLine(retVal);
                    }
                    return "success";
                }
            }
            else
            {
                string path = "logged.txt";

                using (StreamWriter sw = System.IO.File.CreateText(path))
                {
                    sw.WriteLine(retVal);
                }
                return "success";
            }
        }

        private List<string> ReadFromFile(string file)
        {
            List<string> retVal = new List<string>();
            using (StreamReader sr = System.IO.File.OpenText(file))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    retVal.Add(s);
                }
            }
            return retVal;
        }

        private static Model.Client ConvertToModel(string json)
            => Newtonsoft.Json.JsonConvert.DeserializeObject<Model.Client>(json);

        private static string FindClient(List<string> lista, Model.LogInClient logInClient)
        {
            foreach (var item in lista)
            {
                Model.Client client = ConvertToModel(item);
                if (client.username == logInClient.username && client.password == logInClient.password)
                {
                    return client.blocked == true ? "false" : item;
                }
            }
            return "false";
        }
    }
}