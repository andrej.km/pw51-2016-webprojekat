﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/SaveChangedApartman")]
    public class SaveChangedApartmanController : Controller
    {
        #region Fields
        private Interfaces.ISaveChange saveChange = new Functions.SaveChange();
        private readonly string path = "apartmans.txt";
        #endregion

        [HttpPost]
        public void Save(Model.Apartman postApartman) 
            => saveChange.Save(postApartman, path);
    }
}