﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/HostApartmans")]
    public class HostApartmansController : Controller
    {
        [HttpPost]
        public JsonResult AllApatmans(string hostUsername)
        {
            List<string> retVal = ReadFromFile("apartmans.txt");

            return Json(retVal);
        }

        private List<string> ReadFromFile(string file)
        {
            List<string> retVal = new List<string>();
            using (StreamReader sr = System.IO.File.OpenText(file))
            {
                string s = "";
                while ((s = sr.ReadLine()) != null)
                {
                    retVal.Add(s);
                }
            }
            return retVal;
        }
    }
}