﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/Filter")]
    public class FilterController : Controller
    {
        private Interfaces.IConvertToJSON<Model.Apartman> convertToJSONapartman =
            new Functions.ConvertToJSON<Model.Apartman>();
        private Interfaces.IConvertToJSON<Model.Rezervacija> convertToJSONrezervacija =
            new Functions.ConvertToJSON<Model.Rezervacija>();
        private Interfaces.IReadAllFileContent readAllFileContent = new Functions.ReadAllFileContent();
        private Interfaces.IConvertToModelApartman modelApartman = new Functions.ConvertToModelApartman();
        public enum Status
        {
            Neaktivna = 0,
            Aktivna = 1
        };
        [HttpPost]
        public JsonResult Send(Model.Filter filter)
        {
            string apartmansFromTxt = readAllFileContent.Read("apartmans.txt");
            string[] apartmansStr = apartmansFromTxt.Split('\n');
            List<Model.Apartman> apartmas = new List<Model.Apartman>();
            for (int i = 0; i < apartmansStr.Length - 1; i++)
            {
                apartmas.Add(modelApartman.Convert(apartmansStr[i]));
            }
            Model.Apartman.Status status = Model.Apartman.Status.Neaktivna;
            if (filter.apartmanStatus == "Aktivna")
            {
                status = Model.Apartman.Status.Aktivna;
            }
            
            List<Model.Apartman> retList = new List<Model.Apartman>();
            foreach (var item in apartmas)
            {
                if (item.status == status)
                {
                    if (item.type == filter.apartmanType)
                    {
                        if (filter.apartmanAmenitie != null && filter.apartmanAmenitie != "")
                        {
                            foreach (var items in item.amenities)
                            {
                                if (items.naziv == filter.apartmanAmenitie)
                                {
                                    retList.Add(item);
                                }
                            }
                        }
                        else
                        {
                            retList.Add(item);
                        }
                    }
                }
            }

            Model.Rezervacija.Status status1 = Model.Rezervacija.Status.Kreirana;
            if (filter.rezervacijaStatus == "Odbijena")
            {
                status1 = Model.Rezervacija.Status.Odbijena;
            }
            else if (filter.rezervacijaStatus == "Odustanak")
            {
                status1 = Model.Rezervacija.Status.Odustanak;
            }
            else if (filter.rezervacijaStatus == "Prihvacena")
            {
                status1 = Model.Rezervacija.Status.Prihvacena;
            }
            else if (filter.rezervacijaStatus == "Zavrsena")
            {
                status1 = Model.Rezervacija.Status.Zavrsena;
            }

            foreach (var item in apartmas)
            {
                if (item.rezervacije != null)
                {
                    foreach (var item1 in item.rezervacije)
                    {
                        if (item1.status == status1)
                        {
                            retList.Add(item);
                        }
                    }
                }
                
            }

            string retVal = "";

            foreach (var item in retList)
            {
                retVal += convertToJSONapartman.Convert(item) + "?";
            }

            return Json(retVal);
        }
    }
}