﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/Amenities")]
    public class AmenitieController : Controller
    {
        private Interfaces.ISaveCreatedToFile saveToFile = new Functions.SaveCreatedToFile();
        private Interfaces.IConvertToJSON<Model.ApartmanItem> convertToJSON = new Functions.ConvertToJSON<Model.ApartmanItem>();
        private readonly string path = "amenities.txt";
        [HttpPost]
        public void Save(Model.ApartmanItem apartmanItems)
        {
            string json = convertToJSON.Convert(apartmanItems);
            saveToFile.Save(path, json);
        }
    }
}