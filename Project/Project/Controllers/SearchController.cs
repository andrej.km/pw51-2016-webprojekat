﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/Search")]
    public class SearchController : Controller
    {
        private Interfaces.IConvertToJSON<Model.Client> convertToJSONclient =
            new Functions.ConvertToJSON<Model.Client>();
        private Interfaces.IConvertToJSON<Model.Apartman> convertToJSONapartman =
            new Functions.ConvertToJSON<Model.Apartman>();
        private Interfaces.IConvertToJSON<Model.Rezervacija> convertToJSONrezervacija =
            new Functions.ConvertToJSON<Model.Rezervacija>();
        private Interfaces.IReadAllFileContent readAllFileContent = new Functions.ReadAllFileContent();
        private Interfaces.IConvertToModelApartman modelApartman = new Functions.ConvertToModelApartman();
        [HttpPost]
        public JsonResult Search(Model.Search search)
        {
            List<string> allFound = new List<string>();

            string usernameHost = search.username;
            List<Model.Client> clientlist = new List<Model.Client>();

            string gender = search.gender;

            string role = search.role;

            string[] date = search.dateInterval.Split('_');
            List<Model.Apartman> datelist = new List<Model.Apartman>();

            string grad = search.grad;
            List<Model.Apartman> gradlist = new List<Model.Apartman>();

            string[] cenaStr = search.cenaInterval.Split('_');
            double cenaStart = 0;
            Double.TryParse(cenaStr[0], out cenaStart);
            double cenaEnd = 0;
            Double.TryParse(cenaStr[1], out cenaEnd);
            List<Model.Apartman> cenalist = new List<Model.Apartman>();

            string[] brojSobaStr = search.brSobeInterval.Split('_');
            int brojSobaStart = 0;
            Int32.TryParse(brojSobaStr[0], out brojSobaStart);
            int brojSobaEnd = 0;
            Int32.TryParse(brojSobaStr[1], out brojSobaEnd);
            List<Model.Apartman> brojSobalist = new List<Model.Apartman>();

            int brojOsoba = search.brOsoba;
            List<Model.Apartman> brojOsobalist = new List<Model.Apartman>();

            string gost = search.gestUsername;
            List<Model.Rezervacija> gostlist = new List<Model.Rezervacija>();

            string apartmansFromTxt = readAllFileContent.Read("apartmans.txt");
            string clientsFromTxt = readAllFileContent.Read("clients.txt");

            string[] apartmansStr = apartmansFromTxt.Split('\n');
            string[] clientsStr = clientsFromTxt.Split('\n');

            List<Model.Apartman> apartmas = new List<Model.Apartman>();
            List<Model.Client> clients = new List<Model.Client>();

            for (int i = 0; i < apartmansStr.Length - 1; i++)
            {
                apartmas.Add(modelApartman.Convert(apartmansStr[i]));
            }
            for (int i = 0; i < clientsStr.Length - 1; i++)
            {
                clients.Add(ConvertToModel(clientsStr[i]));
            }

            // trazenje klijenta
            foreach (var item in clients)
            {
                if (usernameHost != null)
                {
                    if (usernameHost == item.username)
                    {
                        if (gender == item.sex)
                        {
                            if (role == item.role)
                            {
                                clientlist.Add(item);
                                allFound.Add("client");
                            }
                        }
                    }
                }
                else
                {
                    if (gender == item.sex)
                    {
                        if (role == item.role)
                        {
                            clientlist.Add(item);
                            allFound.Add("client");
                        }
                    }
                }
            }

            foreach (var item in apartmas)
            {
                // grad
                if (grad != null)
                {
                    if (grad == item.location.adresa.naseljenoMesto)
                    {
                        gradlist.Add(item);
                        allFound.Add("grad");
                    }
                }
                // interval datuma
                if (date[0] != "" && date[1] != "")
                {
                    string[] date_0_str = date[0].Split('-');
                    int date0_dan = Int32.Parse(date_0_str[0]);
                    int date0_mesec = Int32.Parse(date_0_str[1]);
                    int date0_godina = Int32.Parse(date_0_str[2]);

                    string[] date_1_str = date[1].Split('-');
                    int date1_dan = Int32.Parse(date_1_str[0]);
                    int date1_mesec = Int32.Parse(date_1_str[1]);
                    int date1_godina = Int32.Parse(date_1_str[2]);

                    string[] temp = item.datumiZaIzdavanje.Split(','); // iz apartmana

                    foreach (var item1 in temp)
                    {
                        string[] item1_str = item1.Split('-');
                        int item1_dan = Int32.Parse(item1_str[0]);
                        int item1_mesec = Int32.Parse(item1_str[1]);
                        int item1_godina = Int32.Parse(item1_str[2]);

                        if (date0_godina < item1_godina && item1_godina < date1_godina) // ako je u intervalu samo izmedju godina
                        {
                            datelist.Add(item);
                            allFound.Add("date");
                            break;
                        }
                        else if (date0_godina == item1_godina || item1_godina == date1_godina)
                        { // ako se poklapa bas sa nekom godinom
                            if (date0_mesec < item1_mesec && item1_mesec < date1_mesec)
                            {
                                datelist.Add(item);
                                allFound.Add("date");
                                break;
                            }
                            else if (date0_mesec == item1_mesec || item1_mesec == date1_mesec)
                            {
                                if (date0_dan <= item1_dan && item1_dan <= date1_dan)
                                {
                                    datelist.Add(item);
                                    allFound.Add("date");
                                    break;
                                }
                            }
                        }
                    }
                }
                // interval cene
                if (cenaStart != 0 || cenaEnd != 0)
                {
                    if (cenaStart < item.cenaPoNoci && item.cenaPoNoci < cenaEnd)
                    {
                        cenalist.Add(item);
                        allFound.Add("cena");
                    }
                }

                // interval broja soba
                if (brojSobaStart != 0 || brojSobaEnd !=0)
                {
                    if (brojSobaStart < item.brojSoba && item.brojSoba < brojSobaEnd)
                    {
                        brojSobalist.Add(item);
                        allFound.Add("brojSoba");
                    }
                }

                // broj osoba
                if (brojOsoba != 0)
                {
                    if (brojOsoba == item.brojGostiju)
                    {
                        brojOsobalist.Add(item);
                        allFound.Add("brojOsoba");
                    }
                }
                if (gost != null)
                {
                    if (item.rezervacije != null)
                    {
                        foreach (var items in item.rezervacije)
                        {
                            if (gost == items.usernameGest)
                            {
                                gostlist.Add(items);
                                allFound.Add("rezervacija");
                            }
                        }
                    }
                }
            }

            bool uListi = false;
            List<Model.Apartman> finalList = new List<Model.Apartman>();

            foreach (var item in apartmas)
            {
                if (allFound.Contains("grad"))
                {
                    uListi = false;
                    foreach (var itemGrad in gradlist)
                    {
                        if (item == itemGrad)
                        {
                            uListi = true;
                            break;
                        }
                    }
                    if (uListi == false)
                    { // ako nisi nasao preskoci iteraciju
                        continue;
                    }
                }
                else if (grad != null)
                {
                    continue;
                }

                if (allFound.Contains("date"))
                {
                    uListi = false;
                    foreach (var itemDate in datelist)
                    {
                        if (item == itemDate)
                        {
                            uListi = true;
                            break;
                        }
                    }
                    if (uListi == false)
                    { // ako nisi nasao preskoci iteraciju
                        continue;
                    }
                }
                else if (date[0] != null && date[1] != null)
                {
                    if (date[0] != "" && date[1] != "")
                    {
                        continue;
                    }
                }

                if (allFound.Contains("cena"))
                {
                    uListi = false;
                    foreach (var itemCena in cenalist)
                    {
                        if (item == itemCena)
                        {
                            uListi = true;
                            break;
                        }
                    }
                    if (uListi == false)
                    { // ako nisi nasao preskoci iteraciju
                        continue;
                    }
                }
                else if (cenaStart != 0 || cenaEnd != 0)
                {
                    continue;
                }

                if (allFound.Contains("brojSoba"))
                {
                    uListi = false;
                    foreach (var itembrojSoba in brojSobalist)
                    {
                        if (item == itembrojSoba)
                        {
                            uListi = true;
                            break;
                        }
                    }
                    if (uListi == false)
                    { // ako nisi nasao preskoci iteraciju
                        continue;
                    }
                }
                else if (brojSobaStart != 0 || brojSobaEnd != 0)
                {
                    continue;
                }

                if (allFound.Contains("brojOsoba"))
                {
                    uListi = false;
                    foreach (var itembrojOsoba in brojOsobalist)
                    {
                        if (item == itembrojOsoba)
                        {
                            uListi = true;
                            break;
                        }
                    }
                    if (uListi == false)
                    { // ako nisi nasao preskoci iteraciju
                        continue;
                    }
                }
                else if (brojOsoba != 0)
                {
                    continue;
                }

                if (uListi == true) // ako se nalazi u svakoj listi
                {
                    finalList.Add(item);
                }
            }

            string retValSve = "";

            if (clientlist.Count != 0)
            {
                foreach (var item in clientlist)
                {
                    retValSve += convertToJSONclient.Convert(item) + "?";
                }
            }
            if (finalList.Count != 0)
            {
                foreach (var item in finalList)
                {
                    retValSve += convertToJSONapartman.Convert(item) + "?";
                }
            }
            if (gostlist.Count != 0)
            {
                foreach (var item in gostlist)
                {
                    retValSve += convertToJSONrezervacija.Convert(item) + "?";
                }
            }

            return Json(retValSve);
        }

        private static Model.Client ConvertToModel(string json)
            => Newtonsoft.Json.JsonConvert.DeserializeObject<Model.Client>(json);
    }
}