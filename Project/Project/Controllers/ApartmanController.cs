﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/Apartman")]
    public class ApartmanController : Controller
    {
        private Interfaces.ISaveCreatedToFile saveCreatedToFile = new Functions.SaveCreatedToFile();

        [HttpPost]
        public void CreateApartman(Model.Apartman apartman)
        {
            string jsonApartman = JsonConvert.SerializeObject(apartman);

            string path = "apartmans.txt";

            saveCreatedToFile.Save(path, jsonApartman);
        }
    }
}