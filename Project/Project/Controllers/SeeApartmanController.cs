﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/SeeApartman")]
    public class SeeApartmanController : Controller
    {
        #region Fields
        private Interfaces.IReadFromFile readFromFile = new Functions.ReadFromFIle();
        private Interfaces.ISaveToFile saveToFile = new Functions.SaveToFile();
        private Interfaces.IConvertToJSON<Model.Apartman> convertToJSON = new Functions.ConvertToJSON<Model.Apartman>();

        private readonly string path = "apartman.txt";
        #endregion

        [HttpPost]
        public void SaveApartman(Model.Apartman apartman) 
            => saveToFile.Save(path, convertToJSON.Convert(apartman));

        [HttpGet]
        public JsonResult GetAprtman() 
            => Json(readFromFile.Read(path));
    }
}