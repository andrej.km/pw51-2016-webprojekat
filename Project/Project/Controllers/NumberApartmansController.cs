﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/NumberApartmans")]
    public class NumberApartmansController : Controller
    {//
        [HttpGet]
        public int GetNumber()
        {
            int retVal = 0;

            string path = "apartmans.txt";

            using (StreamReader sr = new StreamReader(path))
            {
                while (sr.ReadLine() != null)
                {
                    retVal++;
                }
            }

            return retVal;
        }
    }
}