﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Project.Controllers
{
    [Produces("application/json")]
    [Route("api/Sort")]
    public class SortController : Controller
    {
        private Interfaces.IConvertToJSON<Model.Apartman> convertToJSONapartman =
            new Functions.ConvertToJSON<Model.Apartman>();
        private Interfaces.IConvertToJSON<Model.Rezervacija> convertToJSONrezervacija =
            new Functions.ConvertToJSON<Model.Rezervacija>();
        private Interfaces.IReadAllFileContent readAllFileContent = new Functions.ReadAllFileContent();
        private Interfaces.IConvertToModelApartman modelApartman = new Functions.ConvertToModelApartman();

        [HttpPost]
        public JsonResult Send(Model.Sort sort)
        {
            string apartmansFromTxt = readAllFileContent.Read("apartmans.txt");
            string[] apartmansStr = apartmansFromTxt.Split('\n');
            List<Model.Apartman> apartmas = new List<Model.Apartman>();
            for (int i = 0; i < apartmansStr.Length - 1; i++)
            {
                apartmas.Add(modelApartman.Convert(apartmansStr[i]));
            }
            if (sort.apartmanCena != null || sort.apartmanCena != "")
            {
                if (sort.apartmanCena == "Rastuca")
                {
                    for (int i = 1; i < apartmas.Count; i++)
                    {
                        for (int j = 0; j < (apartmas.Count - i); j++)
                        {
                            if (apartmas[j].cenaPoNoci > apartmas[j + 1].cenaPoNoci)
                            {
                                Model.Apartman temp = apartmas[j];
                                apartmas[j] = apartmas[j + 1];
                                apartmas[j + 1] = temp;
                            }
                        }
                    }
                }
                else
                {
                    for (int i = 1; i < apartmas.Count; i++)
                    {
                        for (int j = 0; j < (apartmas.Count - i); j++)
                        {
                            if (apartmas[j].cenaPoNoci < apartmas[j + 1].cenaPoNoci)
                            {
                                Model.Apartman temp = apartmas[j];
                                apartmas[j] = apartmas[j + 1];
                                apartmas[j + 1] = temp;
                            }
                        }
                    }
                }
            }

            if (sort.rezervacijaCena != null || sort.rezervacijaCena != "")
            {
                if (sort.rezervacijaCena == "Rastuca")
                {
                    foreach (var item in apartmas)
                    {
                        if (item.rezervacije != null)
                        {
                            for (int i = 1; i < item.rezervacije.Count; i++)
                            {
                                for (int j = 0; j < (item.rezervacije.Count - i); j++)
                                {
                                    if (Double.Parse(item.rezervacije[j].ukupnaCena) > Double.Parse(item.rezervacije[j + 1].ukupnaCena))
                                    {
                                        Model.Rezervacija temp = item.rezervacije[j];
                                        item.rezervacije[j] = item.rezervacije[j + 1];
                                        item.rezervacije[j + 1] = temp;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    foreach (var item in apartmas)
                    {
                        if (item.rezervacije != null)
                        {
                            for (int i = 1; i < item.rezervacije.Count; i++)
                            {
                                for (int j = 0; j < (item.rezervacije.Count - i); j++)
                                {
                                    if (Double.Parse(item.rezervacije[j].ukupnaCena) < Double.Parse(item.rezervacije[j + 1].ukupnaCena))
                                    {
                                        Model.Rezervacija temp = item.rezervacije[j];
                                        item.rezervacije[j] = item.rezervacije[j + 1];
                                        item.rezervacije[j + 1] = temp;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            string retVal = "";

            foreach (var item in apartmas)
            {
                retVal += convertToJSONapartman.Convert(item) + "?";
            }

            return Json(retVal);
        }
    }
}